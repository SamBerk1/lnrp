$.getJSON('https://ipapi.co/json/', function(data){
    localStorage.setItem('lnrp-ip', data.ip);
})

var exports = {};

window.iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-59987257-2', 'auto');
