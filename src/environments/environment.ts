// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    apiURL: 'https://crate.latenightrecordpool.com:2053/api',
    crateweb: 'https://crate.latenightrecordpool.com:2053',
    dropboxweb: 'https://crate.latenightrecordpool.com:2053',
    dropboxAccount: 'https://api.dropboxapi.com/2/users/get_current_account',
    revokeDropbox: 'https://api.dropboxapi.com/2/sharing/revoke_shared_link',
    jwtsecret: 'KInpGVCKN89WJ1N3uUoGL0zsmkYOUGGRVUavl7QXvUA7Zcq7HUjC1eH406lFiNhz',
    crateqKey: '1f8e1649a2b3cf38c04323020050257078aead7d',
    queueAPIURL: 'https://crateq-dev.azurewebsites.net/api/Content',
    crateQDownloader: 'http://www.crateq.com/lnrp',
};
