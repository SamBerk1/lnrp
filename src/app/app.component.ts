import { Component, OnInit } from '@angular/core';
import { Router, NavigationError, NavigationEnd, RoutesRecognized } from '@angular/router';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { HTTPInterceptor, RouteService, JWTObject } from './services';
import { AuthService } from '../app/pages/auth/auth.service';
import { ReleaseService, CrateQLink } from '../app/pages/release/release.service';
import { LandingComponent, CrateqComponent } from '../app/shared/modals';

import { environment } from '../environments/environment';

import * as $ from 'jquery';
import { AccountService } from './pages/account/account.service';
import { UserAccount } from './utils';

declare var ga:Function;

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    pages_loaded: number = 0;
    ip_address: string = '';
    isBlog: boolean = false;
    currentUrl: string = '';
    openModalRef: BsModalRef;
    modalTitle: string = '';
    modalEnabled: boolean = true;
    modalMessage: string = '';
    isCrateQEnabled: boolean = true;
    
    crateQInfo: CrateQLink;

    isLoaded = false;

    isReturnTop = false;

    constructor(
        private routeServce: RouteService,
        private releaseService: ReleaseService,
        private authService: AuthService,
        private router: Router,
        private newHttp: HTTPInterceptor,
        private modalService: BsModalService,
        private _accountService: AccountService

    ) {
        this.ip_address = localStorage.getItem('lnrp-ip');
    }

    ngOnInit() {
        // Check for a route change and scroll to top
        this.router.events.subscribe((evt) => {
            if (evt instanceof NavigationError) {
                this.router.navigateByUrl('/');
                return;
            }

            if (evt instanceof RoutesRecognized) {

                this.currentUrl = evt.url;
                // Check if route is new to any page but Releases and clear the filter
                if (evt['url'].indexOf('/releases') === -1) {
                    this.releaseService.clearFilter();
                }

                if (evt.url === '/blog') {
                    this.isBlog = true;
                } else {
                    this.isBlog = false;
                }

                if (evt.url === '/signup') {
                    ga('send', 'event', {
                        eventCategory: 'signupClick',
                        eventLabel: 'header',
                        eventAction: 'click'
                    });
                }

                this.routeServce.routeEventBroadcast(evt.url);
            }

            if (!(evt instanceof NavigationEnd)) {
                return;
            }

            // Google Analytics tracking
            ga('set', 'page', evt.url);
            ga('send', 'pageview');

            window.scrollTo(0, 0);

            this.pages_loaded++;
        });

        // Check if a token is loaded
        if (localStorage.getItem('_t') !== null) {
            this.newHttp.updateJWT(
                localStorage.getItem('_t')
            );

            // Check if JWT validated
            if (this.newHttp.jwt.hasOwnProperty('payload')) {
                // Check if JWT has a valid common user value to see if they're loaded
                if ((this.newHttp.jwt['payload']).hasOwnProperty('username')) {
                    // User is currently logged in. Set all existing user information
                    this.authService.processLogin(true);
                    this.updateUserIPaddress();
                }
            }
        }

        this.releaseService.fetchLandingContent().then((res) => {
            if (res.success) {
                this.modalEnabled = res.enabled;
                this.modalTitle = res.title;
                this.modalMessage = res.message;
                this.isCrateQEnabled = res.qenabled;
                this.openLanding();
            }
        })

        // this.releaseService.fetchDownloadLink().then((res: CrateQLink) => {
        //     this.crateQInfo = res;
        //     if (this.isLoaded) {
        //         this.openLanding();
        //     } else {
        //         this.isLoaded = true;
        //     }
        // })

        this.authService.loginAnnounce$.subscribe(() => {

            // user will be taken to the profile view if one of these fields is not filled out:
            // Equipment Used
            // Software Used
            // Controller Used
            // What type of DJ are you?
            // Top 5 Genres You're Looking For
            // Top 5 Artists or Groups
            // Top 5 Producers
            // What has influenced you as a DJ?
            // Top 5 Desert Island Albums
            // How did you hear about us?
            const djDone = this._accountService.djProfileIsConsideredComplete(this.newHttp.jwt.getAccount() as UserAccount);

            if (djDone) {
                this.router.navigateByUrl('/new');
            } else {
                this.router.navigateByUrl('/profile');
            }
        });

        this.releaseService.crateqModal$.subscribe(() => {
            this.modalService.show(CrateqComponent, { class: 'detail-form' });
            this.releaseService.CQDownloads = 1;
        })
    }

    ngAfterViewInit() {
        $(window).scroll(() => {
            const hero_height = $('#front-hero').height() || 750;
            const scroll = $(window).scrollTop();
            if (scroll > hero_height) {
                this.isReturnTop = true;
            } else {
                this.isReturnTop = false;
            }
        });
    }

    scrollToTop = () => {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    }

    updateUserIPaddress() {
        this.newHttp.post(environment.apiURL + '/user/ip', {'ip_address': this.ip_address});
    }

    openLanding() {
        const landing = localStorage.getItem('lnrp-landing');
        if (landing !== 'true' && this.modalEnabled) {
            const config: ModalOptions = {
                class: 'landing-modal',
                animated: false,
                keyboard: false,
                ignoreBackdropClick: true,
            };
            this.openModalRef = this.modalService.show(LandingComponent, config);
            this.openModalRef.content.title = this.modalTitle;
            this.openModalRef.content.message = this.modalMessage;
            // this.openModalRef.content.crateQLink = this.crateQInfo;
            this.openModalRef.content.isCrateQEnabled = this.isCrateQEnabled;
        } else {
            this.openCrateQ();
        }
    }

    openCrateQ() {
        const isCrateQ = localStorage.getItem('lnrp-crateq');
        if (isCrateQ !== 'true' && this.isCrateQEnabled) {
            this.modalService.show(CrateqComponent, { class: 'detail-form' });
        }
    }
}
