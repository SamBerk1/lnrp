import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'time' })
export class TimePipe {
    constructor() { }

    transform(seconds) {
        seconds = Math.floor(seconds);

        if (!Number.isInteger(seconds)) {
            return '00:00';

        } else {
            return moment.utc(0)
                         .seconds(seconds)
                         .format('mm:ss');
        }
    }
}
