import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'slug' })
export class SlugPipe {
    constructor() {
    }

    transform(style) {
        return style.replace(/ /g, '-').replace(/[^\w-]+/g, '');
    }
}
