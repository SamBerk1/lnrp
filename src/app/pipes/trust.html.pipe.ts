import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer  } from '@angular/platform-browser';

@Pipe({ name: 'safeHTML' })
export class SanitizerHTMLPipe {
    constructor (
        private sanitizer: DomSanitizer
    ) {}

    transform(v: string) {
        return this.sanitizer.bypassSecurityTrustHtml(v);
    }
}
