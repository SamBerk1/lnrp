import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'concatenate' })
export class ConcatenatePipe {
    constructor() {
    }

    transform(style) {
        return style.replace(/[\s/&]/g, '');
    }
}
