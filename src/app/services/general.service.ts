import { Injectable } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

import { environment } from '../../environments/environment';

import { HTTPInterceptor } from './interceptor.service';
import { ErrorHelper } from './error.service';

import { UserAccount } from '../utils';

import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/catch';

@Injectable()
export class GeneralService {
    private toggleDesktopSource = new Subject<boolean>();
    toggleDesktop$ = this.toggleDesktopSource.asObservable();

    isDesktop: boolean = false;

    constructor(
        private newHttp: HTTPInterceptor,
        private _error: ErrorHelper,
        private deviceService: DeviceDetectorService,
    ) {
        this.initial();
    }

    initial = () => {
        this.isDesktop = this.deviceService.isDesktop();
        this.toggleDesktopSource.next(this.isDesktop);
    }

    getFAQContent = () => {
        return this.newHttp.get(environment.apiURL + '/content/faq')
                   .map(response => response.json())
                   .catch(this._error.handleError);
    }

    sendContactForm = (values: Object) => {
        return this.newHttp.post(environment.apiURL + '/contact', values)
                   .map(response => response.json())
                   .catch(this._error.handleError);
    }

    sendRenewRequest = (user: UserAccount, creditCard: Object) => {
        return this.newHttp
                   .put(`${environment.apiURL}/user/renew/${user['renew_code']}`, creditCard)
                   .catch(this._error.handleError);
    }

    toggleDesktop = () => {
        this.isDesktop = !this.isDesktop;
        this.toggleDesktopSource.next(this.isDesktop);
    }
}
