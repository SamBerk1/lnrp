import { environment } from '../../environments/environment';

declare var KJUR: any;
declare var stohex: any;

export class JWTObject {
    private _jwa: any;
    private _secret: string = environment.jwtsecret;

    private raw_jwt: string;

    private raw_header: string;
    private raw_payload: string;
    private signature: string;

    private header: string | Object;
    private payload: string | Object;

    constructor() {}

    get accountLoaded(): Boolean {
        return typeof this.payload === 'object';
    }

    from = (jwt: string) => {
        // Decode the JWT and parse out the header and payload
        const jwt_decoded = KJUR.jws.JWS.parse(jwt);

        // Build out a new JWT signed here to compare against
        const jwt_compare = KJUR.jws.JWS.sign('HS256', jwt_decoded.headerObj, jwt_decoded.payloadObj, stohex(this._secret));

        // Compare against provided JWT
        // if (jwt !== jwt_compare) {
            // The JWT value does not have a valid signature. Shut that shit down
            // console.error('This JWT does not have a valid signature! We really need to do something about that...');

        // } else {
            // The JWT validates successfully!
            // Assign the parsed header and payload objects
            this.header = jwt_decoded.headerObj;
            this.payload = jwt_decoded.payloadObj;
        // }
    }

    toJWT = (): string => {
        if (!this.payload) {
            return '';
        }

        const cloned_user = JSON.parse(JSON.stringify(this.payload));
        delete cloned_user['versions'];
        delete cloned_user['releases'];

        // Combine strings, sign and return
        return KJUR.jws.JWS.sign('HS256', this.header, cloned_user, stohex(this._secret));
    }

    getAccount = (): Object => {
        if (typeof this.payload === 'object') {
            return this.payload;
        } else {
            return false;
        }
    }
}
