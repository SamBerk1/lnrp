import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { HTTPInterceptor } from './interceptor.service';

import { environment } from '../../environments/environment';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

@Injectable()

export class GenreService {
    genres: Object[] = [];

    constructor(
        // private http: Http,
        private newHttp: HTTPInterceptor
    ) {}

    private handleError(error: any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    private extractJSON(res: Response) {
        let json = res.json();
        return json._body || '';
    }

    public getParentGenres() {
        return this.newHttp.get(environment.apiURL + '/genres/parents')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    public getParentGenresByReleaseCount() {
        return this.newHttp.get(environment.apiURL + '/genres/parents?rank=releases')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    public getAllGenres() {
        return this.newHttp.get(environment.apiURL + '/genres')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    public getAllSubgenres(rank?: string) {
        rank = rank === undefined ? '' : '?rank=' + rank;

        return this.newHttp.get(environment.apiURL + '/genres/subgenres' + rank)
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    public getDecades() {
        return this.newHttp.get(environment.apiURL + '/genres/decades')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    public getThrowbacks() {
        return this.newHttp.get(environment.apiURL + '/genres/throwbacks')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    loadGenres = () => {
        if (this.genres.length === 0) {
            this.newHttp.get(environment.apiURL + '/genres')
                .toPromise()
                .then((genres) => this.genres = genres.json());
        }
    }
}
