import {
    Injector,
    Injectable,
    Provider
} from '@angular/core';

import { AuthService } from '../pages/auth/auth.service';

import { ToastrService } from 'ngx-toastr';

import { Observable } from 'rxjs/Observable';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Injectable()
export class ErrorHelper {
    constructor(
        private authService: AuthService,
        private toastService: ToastrService
    ) {}

    handleError = (error: any) => {
        if (!error.ok) {
            switch (error.status) {
                case 400:
                    this.showErrorMessage('An error occurred trying to perform this action. Please try again or contact support.');
                    break;

                case 401:
                    this.authService.registerLogout();
                    this.showErrorMessage('Your login has expired. Please log back in and try again.');
                    break;

                case 404:
                    this.showErrorMessage(error._body);
                    break;

                case 500:
                    this.showErrorMessage(error._body);
                    break;

                default:
                    // Do nothing, yet.
                    break;
            }
        }

        return Observable.throw(error._body || error);
    }

    showErrorMessage(errMsg: string) {
        this.toastService.error(errMsg);
    }

}

export function errorLoader(authService: AuthService, toastService: ToastrService) { 
    return new ErrorHelper(authService, toastService);
}

export const ERROR_HELPER_PROVIDERS: Provider[] = [
    {
        provide: ErrorHelper,
        deps: [AuthService],
        useFactory: errorLoader
    }
];
