import {
    Injector,
    Injectable,
    Provider
} from '@angular/core';

import {
    Http,
    Request,
    RequestOptions,
    RequestOptionsArgs,
    RequestMethod,
    Response,
    Headers,
    XHRBackend,
    ConnectionBackend
} from '@angular/http';

import { JWTObject } from './jwtobject';

import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Observable, ObservableInput } from 'rxjs/Observable';

import 'rxjs/add/operator/do';

@Injectable()
export class HTTPInterceptor extends Http {

    public jwt: JWTObject;

    constructor(
        private backend: ConnectionBackend,
        private defaultOptions: RequestOptions,
        private progressBar: SlimLoadingBarService
    ) {
        super(backend, defaultOptions);

        this.jwt = new JWTObject();

        this.progressBar = progressBar;
    }

    updateJWT = (jwt: string) => {
        // TODO: this is defensive code. Some error in the JWT storing
        // Trolling logs now to find recreation steps
        if (jwt === '') {
            localStorage.removeItem('_t');

            return false;
        }

        // Create temporary JWTObject to validate
        const temp_jwt = new JWTObject();
            temp_jwt.from(jwt);

        // Get the current date/time
        const d = new Date();
        const current_time = Math.floor(d.getTime() / 1000);

        // Check if JWTObject has expired
        // TODO: Add more verifications
        if (current_time >= temp_jwt['payload']['exp']) {
            // JWTObject has expired
        } else {
            // JWTObject has passed validation. Convert to official JWTObject
            this.jwt.from(jwt);
        }
    }

    request (url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        const req: Request = url as Request;
        const token: string = this.jwt.toJWT();

        // Update local storage
        this.updateStoredJWT(token);

        if (token === null) {
            super.request(url, options);
        }

        return this.requestWithToken(req, token);
    }

    get (url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(this.requestHelper({ body: '', url, method: RequestMethod.Get }, this.getRequestOptionArgs()));
    }

    post (url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
      return this.intercept(this.requestHelper({ body, url, method: RequestMethod.Post }, this.getRequestOptionArgs()));
    }

    put (url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(this.requestHelper({ body, url, method: RequestMethod.Put }, this.getRequestOptionArgs()));
    }

    delete (url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(this.requestHelper({ body: '$1', url, method: RequestMethod.Delete }, this.getRequestOptionArgs()));
    }

    refreshStoredUser = (user: Object) => {
        this.jwt['payload'] = user;

        const token: string = this.jwt.toJWT();

        if (token === '') {
            return false;
        }

        this.updateStoredJWT(token);
    }

    private getRequestOptionArgs = (options?: RequestOptionsArgs): RequestOptionsArgs => {
        if (options == null) {
            options = new RequestOptions();
        }

        if (options.headers == null) {
            options.headers = new Headers();
        }

        options.headers.append('Content-Type', 'application/json');

        return options;
    }

    private mergeOptions = (providedOpts: RequestOptionsArgs, defaultOpts?: RequestOptions) => {
      let newOptions = defaultOpts || new RequestOptions();

      newOptions = newOptions.merge(new RequestOptions(providedOpts));

      return newOptions;
    }

    private requestHelper = (requestArgs: RequestOptionsArgs, additionalOptions?: RequestOptionsArgs): Observable<Response> => {
        let options = new RequestOptions(requestArgs);

        if (additionalOptions) {
            options = options.merge(additionalOptions);
        }

        return this.request(new Request(this.mergeOptions(options, this.defaultOptions)));
    }

    private requestWithToken = (req: Request, token: string): Observable<Response> => {
        req.headers.set('Authorization', 'Bearer ' + token);

        return super.request(req);
    }

    private updateStoredJWT = (jwt: string) => {
        localStorage.setItem('_t', jwt);
    }

    private intercept = (observable: Observable<Response>): Observable<Response> => {
        this.progressBar.start();

        return observable.do(
            (res) => {
                const response = JSON.parse(res['_body']);

                if (response.hasOwnProperty('_t')) {
                    this.updateStoredJWT(response['_t']);

                    this.jwt.from(response['_t']);
                }
            },
            (err) => {
                this.progressBar.complete();
            },
            () => {
                this.progressBar.complete();
            }
        );
    }
}

export function interceptorLoader(backend: XHRBackend, defaultOptions: RequestOptions, slim: SlimLoadingBarService) {
    return new HTTPInterceptor(backend, defaultOptions, slim);
}

export const INTERCEPTOR_PROVIDERS: Provider[] = [
    {
        provide: HTTPInterceptor,
        deps: [XHRBackend, RequestOptions, SlimLoadingBarService],
        useFactory: interceptorLoader
    }
];
