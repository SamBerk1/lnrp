import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class RouteService {

    private routeEventSource = new Subject<string>();
    releaseEvent = this.routeEventSource.asObservable();

    constructor(
    ) {}

    routeEventBroadcast = (routeUrl: string) => {
        this.routeEventSource.next(routeUrl);
    }
}
