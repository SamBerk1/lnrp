import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../pages/auth/auth.service';
import { ReleaseService } from '../../pages/release/release.service';
import { GenreService, RouteService, GeneralService } from '../../services';

import * as $ from 'jquery';

@Component({
    selector: 'site-footer',
    templateUrl: './footer.component.html'
})

export class FooterComponent implements OnInit, AfterViewInit {

    parent_genres: any;

    searchTerm: string;

    isBlog: boolean = false;

    isDesktop: boolean = false;

    constructor(
        private genreService: GenreService,
        private releaseService: ReleaseService,
        private authService: AuthService,
        private routeService: RouteService,
        private generalService: GeneralService,
        private _router: Router
    ) {
        this.isDesktop = this.generalService.isDesktop;

        this.generalService.toggleDesktop$.subscribe(isDesktop => {
            this.isDesktop = isDesktop;
        })
    }

    ngOnInit() {
        this.releaseService.clearSearchTextReload$
            .subscribe(
                () => {
                    this.searchTerm = '';
                }
            );

        this.genreService.getParentGenres()
            .then((response) => {
                this.parent_genres = response;
            });

        this.routeService.releaseEvent.subscribe(
            (route) => {
                if (route === '/blog') {
                    this.isBlog = true;
                } else {
                    this.isBlog = false;
                }
            }
        );
    }

    ngAfterViewInit() { }

    get isLoggedIn(): Boolean {
        return this.authService.isLoggedIn;
    }

    openLogin = (evt: any) => {
        evt.preventDefault();

        this.authService.openLoginModal();
    }

    openRenew = (evt: any) => {
        evt.preventDefault();

        this.authService.openRenewModal();
    }

    setGenre = (id: Number, genre: string) => {
        this.releaseService.setGenre(id, genre);

        this.footerClickCleanup();
    }

    onSubmit(form: any) {
        this.releaseService.setSearch(this.searchTerm);
        this.releaseService.page_title = 'Search Results';

        this.footerClickCleanup();
    }

    private footerClickCleanup = () => {
        if (this._router.routerState.snapshot.url === '/releases') {
            this.releaseService.requestReleasesBroadcast();

            document.body.scrollTop = 0;

        } else {
            this._router.navigate(['releases']);
        }
    }

    moveToScroll = (id) => {
        const el = document.getElementById(id);
        if (el) {
            el.scrollIntoView({ block: 'start', behavior: 'smooth' });
        } else {
            this._router.navigateByUrl('/');
            setTimeout(() => {
                const element = document.getElementById(id);
                if (element) element.scrollIntoView({ block: 'start', behavior: 'smooth' });
            }, 100);
        }
    }

    toggleDesktop = (evt: any) => {
        evt.preventDefault();

        this.generalService.toggleDesktop();

        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    }
}
