import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DeviceDetectorModule } from 'ngx-device-detector';

import { ConcatenatePipe } from './../pipes';

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CrateqComponent, LandingComponent } from './modals';

import { appRouting } from '../app.routes';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        appRouting,
        DeviceDetectorModule.forRoot(),
    ],
    declarations: [
        HeaderComponent,
        FooterComponent,
        ConcatenatePipe,
        CrateqComponent,
        LandingComponent,
        LoaderComponent,
    ],
    exports: [
        HeaderComponent,
        FooterComponent,
        CrateqComponent,
        LandingComponent,
        LoaderComponent,
        ConcatenatePipe,
        CommonModule,
        FormsModule,
        DeviceDetectorModule,
    ],
    entryComponents: [
        CrateqComponent,
        LandingComponent,
    ],
})
export class SharedModule { }
