import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

import { CrateQLink } from '../../../pages/release/release.service';
import { CrateqComponent } from '../crateq/crateq.component';

@Component({
    selector: 'landing-modal',
    templateUrl: './landing.component.html'
})
export class LandingComponent implements OnInit {

    crateqModalRef: BsModalRef;
    isContinue: boolean = false;
    isChecked: boolean = false;

    title: string = '';
    message: string = '';
    isCrateQEnabled: boolean = false;

    // crateQLink: CrateQLink;
    isCrateQ: string = '';

    constructor(
        private bsModalRef: BsModalRef,
        private modalService: BsModalService,
    ) {
        this.isCrateQ = localStorage.getItem('lnrp-crateq');
    }

    ngOnInit() {
        this.modalService.onHidden.take(1).subscribe((res) => {
            if (this.isContinue && this.isCrateQ !== 'true' && this.isCrateQEnabled) {
                this.crateqModalRef = this.modalService.show(CrateqComponent, { class: 'detail-form' });
                // this.crateqModalRef.content.crateQLink = this.crateQLink;
            }
            localStorage.setItem('lnrp-landing', this.isChecked ? 'true' : 'false');
        })
    }

    modalClose = (state: boolean) => {
        this.isContinue = state;
        this.bsModalRef.hide();
    }
}