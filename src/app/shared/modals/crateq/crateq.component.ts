import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { environment } from '../../../../environments/environment';

import { CrateQLink } from '../../../pages/release/release.service';

@Component({
    selector: 'crateq-modal',
    templateUrl: './crateq.component.html'
})
export class CrateqComponent implements OnInit {

    isChecked: boolean = false;
    crateQLink: CrateQLink;

    constructor(
        private bsModalRef: BsModalRef,
        private modalService: BsModalService,
    ) { }

    ngOnInit() {
        this.modalService.onHidden.take(1).subscribe((res) => {
            localStorage.setItem('lnrp-crateq', this.isChecked ? 'true' : 'false');
        })
    }

    closeModal = () => {
        this.bsModalRef.hide();
    }

    download = () => {
        window.open(environment.crateQDownloader, '_blank');
    }
}