import { Component, OnInit, AfterViewInit, trigger, state, style, transition, animate } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../pages/auth/auth.service';
import { ReleaseService } from '../../pages/release/release.service';
import { GenreService, RouteService, GeneralService } from '../../services';

import * as $ from 'jquery';

@Component({
    selector: 'site-header',
    templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit, AfterViewInit {

    isBlog: boolean = false;
    isDesktop: boolean = false;
    header_genres: any;
    header_decades: any;
    header_throwbacks: any;

    page: string;

    searchTerm: string;

    show: any = {
        header_menu: {
            genre: false,
            decade: false,
            throwback: false
        },
        header_decade: false,
        header_throwback: false,
        header_sticky : false,
        header_search : false,
        header_search_items : {
            genre: false,
            decade: false,
            throwback: false
        },
        header_slideout: false
    };

    open_menu: any;

    position = $(window).scrollTop();
    isScrollUp = true;

    constructor(
        private routeService: RouteService,
        private genreService: GenreService,
        private authService: AuthService,
        private releaseService: ReleaseService,
        private generalService: GeneralService,
        private _router: Router,
    ) {
        this.isDesktop = this.generalService.isDesktop;
    }

    ngOnInit() {
        this.releaseService.clearSearchTextReload$
            .subscribe(
                () => {
                    this.searchTerm = '';
                }
            );

        this.genreService.getParentGenresByReleaseCount()
            .then((response) => {
                this.header_genres = response;
            });

        this.genreService.getDecades()
            .then((response) => {
                this.header_decades = response;
            });

        this.genreService.getThrowbacks()
            .then((response) => {
                this.header_throwbacks = response;
            });
        
        this.generalService.toggleDesktop$.subscribe(isDesktop => {
            this.isDesktop = isDesktop;
        })

        this.routeService.releaseEvent.subscribe(
            (route) => {
                this.page = route;
                if (route === '/blog') {
                    this.isBlog = true;
                } else {
                    this.isBlog = false;
                }
            }
        );
    }

    ngAfterViewInit() {
        this.stickyWatcher(this);
    }

    get isLoggedIn(): Boolean {
        return this.authService.isLoggedIn;
    }

    openRenew = (evt: any) => {
        evt.preventDefault();

        this.authService.openRenewModal();
    }

    clickPopupMenu(event, type) {
        let context = this;
        let original_state = this.show['header_menu'][type].toString();

        $.each(this.show['header_menu'], function(key, val) {
            context.show['header_menu'][key] = false;
        });

        if (original_state === 'false') {
            this.show['header_menu'][type] = true;
        }
    }

    closePopupMenus() {
        this.show['header_menu']['genre'] = false;
        this.show['header_menu']['decade'] = false;
        this.show['header_menu']['throwback'] = false;
    }

    clickSearchMenu(event, type) {
        let context = this;
        let original_state = this.show['header_search_items'][type].toString();

        $.each(this.show['header_search_items'], function(key, val) {
            context.show['header_search_items'][key] = false;
        });

        if (original_state === 'false') {
            this.show['header_search_items'][type] = true;
        }
    }

    stickyWatcher(context) {
        let header_height = $('header').height() * .9;

        $(window).scroll(function() {
            const scroll = $(window).scrollTop();
            if (scroll > header_height) {
                context.show['header_sticky'] = true;

            } else {
                context.show['header_sticky'] = false;

                context.show['header_search'] = false;
                context.show['header_search_items']['genre'] = false;
                context.show['header_search_items']['decade'] = false;
                context.show['header_search_items']['throwback'] = false;

                context.show['header_slideout'] = false;
            }

            if (scroll > context.position) {
                if (scroll > 100) {
                    context.isScrollUp = false;
                }
            } else {
                if (scroll > 100) {
                    context.isScrollUp = true;
                }
            }

            context.position = scroll;
        });
    }

    logout = (evt: any) => {
        evt.preventDefault();

        this.authService.doLogout();
    }

    triggerLoginModal = (evt: any) => {
        evt.preventDefault();
        this.show.header_slideout = false;
        this.authService.openLoginModal();
    }

    onSubmit = (form: any) => {

        if (!this.searchTerm || !this.searchTerm.trim()) {
            this.searchTerm = '';
            return;
        }

        this.releaseService.setSearch(this.searchTerm);
        this.releaseService.page_title = 'Search Results';
        this.headerClickCleanup();
    }

    setGenre = (id: Number, genre: string) => {
        this.releaseService.clearFilter();
        this.releaseService.setGenre(id, genre);
        this.headerClickCleanup();
    }

    setDecade = (decade: Number, year: string) => {
        this.releaseService.clearFilter();
        this.releaseService.setDecade(decade, year);
        this.headerClickCleanup();
    }

    setThrowback = (id: Number, genre: string) => {
        this.releaseService.clearFilter();
        this.releaseService.setThrowback(id, genre);
        this.headerClickCleanup();
    }

    goLink = (id: string) => {
        this.show.header_slideout = false
        if (id === '/') {
            $('html, body').animate({
                scrollTop: 0
            }, 1000);

            this.generalService.initial();
        } else {
            const el = document.getElementById(id);
            if (el) {
                el.scrollIntoView({ block: 'start', behavior: 'smooth' });
            } else {
                this._router.navigateByUrl('/');
                setTimeout(() => {
                    const element = document.getElementById(id);
                    if (element) element.scrollIntoView({ block: 'start', behavior: 'smooth' });
                }, 100);
            }
        }
    }

    toggleDesktop = (evt: any) => {
        evt.preventDefault();
        this.show.header_slideout = false
        this.generalService.toggleDesktop();
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    }

    private headerClickCleanup = () => {
        this.closePopupMenus();

        if (this._router.routerState.snapshot.url === '/releases') {
            this.releaseService.requestReleasesBroadcast();

            document.body.scrollTop = 0;

        } else {
            this._router.navigate(['releases']);
        }
    }
}
