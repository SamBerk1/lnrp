export const SearchTermRules = [
    {find: '2 pac', replace: '2pac'},
    {find: 'gangstarr', replace: 'gang starr'}
] as SearchTermRule[];

export class SearchTermRule {
    public find: string;
    public replace: string;
}
