export class ReleaseSearchFilter {
    private genre_id: number[] = [];
    private subgenre_id: number[] = [];
    private label: string[] = [];
    private letter: string[] = [];
    private decade: string[] = [];
    private version: string[] = [];
    private bpm: string[] = [];
    private date: string[] = [];
    private throwback: any = null; // true, false, or null
    private minYear: any = null;
    private maxYear: any = null;
    private remix: any; // true, false, or null
    private search: string[] = [];

    private sortValue: string = '';

    isModified: Boolean = false;

    constructor(args?: Object) {
        if (args !== undefined) {
            this.params = args;
        }
    }

    get params(): Object {
        return {
            genre_id: this.genre_id,
            subgenre_id: this.subgenre_id,
            label: this.label,
            letter: this.letter,
            decade: this.decade,
            minYear: this.minYear,
            maxYear: this.maxYear,
            version: this.version,
            bpm: this.bpm,
            date: this.date,
            throwback: this.throwback,
            remix: this.remix,
            search: this.search,
            sort: this.sortValue
        };
    }

    set params(args: Object) {
        this.isModified = true;

        this.genre_id = args['genre_id'] || [];
        this.subgenre_id = args['subgenre_id'] || [];
        this.label = args['label'] || [];
        this.letter = args['letter'] || [];
        this.decade = args['decade'] || [];
        this.version = args['version'] || [];
        this.bpm = args['bpm'] || [];
        this.date = args['date'] || [];
        this.minYear = args['minYear'] || null;
        this.maxYear = args['maxYear'] || null;
        this.throwback = args['throwback'] || null;
        this.remix = args['remix'] || null;
        this.search = args['search'] || [];
    }

    set sort(sortValue: string) {
        this.sortValue = sortValue;
    }
}
