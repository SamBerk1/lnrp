export const ReleaseConstants = {
    sort_options: [
        { id: "artist", text: "Alphabetical by Artist" },
        { id: "title", text: "Alphabetical by Song" },
        { id: "bpm", text: "Fastest BPM" },
        { id: "bpm_slow", text: "Slowest BPM" },
        { id: "year_new", text: "Year Released (New)" },
        { id: "year_old", text: "Year Released (Old)" },
        { id: "rating", text: "Highest Rated" },
        { id: "votes", text: "Number of Votes" },
        { id: "date", text: "Newest Releases" },
        { id: "date_old", text: "Oldest Releases" }
    ],

    decades: [
        { year: 1970, name: "1970's", selected: false },
        { year: 1980, name: "1980's", selected: false },
        { year: 1990, name: "1990's", selected: false },
        { year: 2000, name: "2000's", selected: false },
        { year: 2010, name: "2010's", selected: false }
    ],

    versions: [
        { id: "clean", name: "Clean", selected: false },
        { id: "dirty", name: "Dirty", selected: false },
        { id: "inst", name: "Instrumental", selected: false },
        { id: "acc", name: "Acapella", selected: false },
        { id: "lnrp mixshow edit (clean)", name: "Clean Mixshow", selected: false },
        { id: "lnrp mixshow edit (dirty)", name: "Dirty Mixshow", selected: false }
    ],

    bpm: [
        { id: "less_80", name: "< 80", selected: false },
        { id: "80_100", name: "80 - 100", selected: false },
        { id: "100_140", name: "100 - 140", selected: false },
        { id: "140_great", name: "140 +", selected: false }
    ],

    letters: [
        { letter: "A", selected: false },
        { letter: "B", selected: false },
        { letter: "C", selected: false },
        { letter: "D", selected: false },
        { letter: "E", selected: false },
        { letter: "F", selected: false },
        { letter: "G", selected: false },
        { letter: "H", selected: false },
        { letter: "I", selected: false },
        { letter: "J", selected: false },
        { letter: "K", selected: false },
        { letter: "L", selected: false },
        { letter: "M", selected: false },
        { letter: "N", selected: false },
        { letter: "O", selected: false },
        { letter: "P", selected: false },
        { letter: "Q", selected: false },
        { letter: "R", selected: false },
        { letter: "S", selected: false },
        { letter: "T", selected: false },
        { letter: "U", selected: false },
        { letter: "V", selected: false },
        { letter: "W", selected: false },
        { letter: "X", selected: false },
        { letter: "Y", selected: false },
        { letter: "Z", selected: false },
        { letter: "0", selected: false },
        { letter: "1", selected: false },
        { letter: "2", selected: false },
        { letter: "3", selected: false },
        { letter: "4", selected: false },
        { letter: "5", selected: false },
        { letter: "6", selected: false },
        { letter: "7", selected: false },
        { letter: "8", selected: false },
        { letter: "9", selected: false }
    ],

    dates: [
        { id: "-1 WEEK", name: "Last Week", selected: false },
        { id: "-1 MONTH", name: "Last Month", selected: false },
        { id: "-3 MONTH", name: "Last 3 Months", selected: false },
        { id: "-6 MONTH", name: "Last 6 Months", selected: false },
        { id: "-1 YEAR", name: "Last Year", selected: false }
    ],

    years: []
};
