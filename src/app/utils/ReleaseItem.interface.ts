export interface ReleaseItem {
    id: number;
    remix: number;
    artist: string;
    title: string;
    time: string;
    year: number;
    bpm: string;
    image: string;
    label: string;
    comments: string;
    versions: any;
    ratings: number;
    remixes: any;
    score: number;
    rating: any;
    rated: boolean;
    downloaded: boolean;
    certified: boolean;
    crateq: boolean;
}
