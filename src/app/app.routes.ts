import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
    FrontpageComponent,
    TopDownloadComponent,
    NewReleaseComponent,
    ReleaseComponent,
    PricingComponent,
    SignupComponent,
    AccountComponent,
    ContactComponent,
    FAQComponent,
    BlogComponent,
    TrendComponent,
    RenewAccountComponent,
    AdvancedSearchComponent,
    ResetPasswordComponent,
    ProfileComponent,
} from './pages';
import { FeaturesComponent } from './pages/features/features.component';

const appRoutes: Routes = [
    {
        path: '',
        component: FrontpageComponent
    }, {
        path: 'releases',
        component: ReleaseComponent
    }, {
        path: 'releases/:page',
        component: ReleaseComponent
    }, {
        path: 'top',
        component: TopDownloadComponent
    }, {
        path: 'top/:genre',
        component: TopDownloadComponent
    }, {
        path: 'top/:genre/:subgenre',
        component: TopDownloadComponent
    }, {
        path: 'new',
        component: NewReleaseComponent
    }, {
        path: 'pricing',
        component: PricingComponent
    },{
        path: 'features',
        component: FeaturesComponent
    },{
        path: 'signup',
        component: SignupComponent
    }, {
        path: 'profile',
        component: ProfileComponent
    }, {
        path: 'account',
        component: AccountComponent
    }, {
        path: 'account/:refresh',
        component: AccountComponent
    }, {
        path: 'advanced',
        component: AdvancedSearchComponent
    }, {
        path: 'contact',
        component: ContactComponent
    }, {
        path: 'renew-account/:code',
        component: RenewAccountComponent
    }, {
        path: 'faq',
        component: FAQComponent
    }, {
        path: 'password-reset/:code',
        component: ResetPasswordComponent
    }, {
        path: 'blog',
        component: BlogComponent
    }, {
        path: 'trending',
        component: TrendComponent
    }, {
        path: '**',
        component: FrontpageComponent
    }
];

export const appRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);
