import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap';

import { environment } from '../../../environments/environment';

import { GeneralService, JWTObject, HTTPInterceptor } from '../../services';
import { AuthService } from '../auth/auth.service';
import { SignupService, Billing } from '../signup/signup.service';
import { PromoModalComponent } from './promo-modal/promo-modal.component';

import { UserAccount } from '../../utils'

@Component({
    selector: 'renew-account',
    templateUrl: './renew.component.html'
})
export class RenewAccountComponent implements OnInit {

    jwt: JWTObject;
    account: any;

    userAccount: UserAccount;

    paymentForm: FormGroup;

    customBilling: any = {}
    isNewBilling: boolean = false;

    creditCard: any = {
        cardNumber: '',
        cardExpMonth: '05',
        cardExpYear: '2019',
        cardCVV: '',
        cardZip: ''
    };

    renew_code: string = '';

    constructor(
        private fb: FormBuilder,
        private _service: GeneralService,
        private authService: AuthService,
        private signupService: SignupService,
        private toastService: ToastrService,
        private advancedRoute: ActivatedRoute,
        private modalService: BsModalService,
        private router: Router,
    ) {
        this.setupPaymentForm();

        const userId =  parseInt(localStorage.getItem('lnrp-user'));
        this.userAccount = signupService.generateNewUser();
        this.userAccount.id = userId;
    }

    ngOnInit() {
        this.advancedRoute.params.subscribe((params) => {
            this.userAccount['renew_code'] = params['code'] || '';
        });

        this.signupService.billingSource$.subscribe(res => {
            this.customBilling = res;
            this.userAccount.subscription = res.stripe_id;
        })
    }

    completeRenew = () => {
        if (this.paymentForm.invalid) {
            this.toastService.error('Your form is not complete. Please check the information entered and try again');
            return;
        }

        this.creditCard['subscription'] = this.userAccount.subscription;

        this._service.sendRenewRequest(this.userAccount, this.creditCard)
            .subscribe(
                (success) => {
                    this.authService.isLoggedIn = true;
                    this.router.navigateByUrl('/');
                    this.toastService.success('Your account has been successfully renewed! Thank you for choosing Late Night Record Pool');
                },
                (err) => {
                    console.error(err);
                }
            );
    }

    openPromoModal() {
        const initialState = {
            code: this.userAccount['renew_code']
        }
        this.modalService.show(PromoModalComponent, { class: 'landing-modal promo-modal', initialState });
    }

    private setupPaymentForm = () => {
        this.paymentForm = this.fb.group({
            paymentType: 'Stripe',
            subscription: 'lnrp_monthly',
            cardNumber: new FormControl(null, Validators.required),
            cardExpMonth: new FormControl('05', Validators.required),
            cardExpYear: new FormControl('2019', Validators.required),
            cardCVV: new FormControl(null, Validators.required),
            cardZip: new FormControl(null, Validators.required)
        });
    }

    // sendResetForm = (form: any) => {
    //     this._service.resetPassword(form)
    //         .subscribe(
    //             (success) => {
    //                 this.notificationService.create({ message: 'Your password has been successfully reset!', type: NotificationType.Success });
    //
    //                 this.router.navigateByUrl('/');
    //             }
    //         )
    // }
}
