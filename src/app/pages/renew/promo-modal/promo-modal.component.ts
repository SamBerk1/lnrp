import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SignupService } from '../../signup/signup.service';

@Component({
    selector: 'app-promo-modal',
    templateUrl: './promo-modal.component.html',
})
export class PromoModalComponent implements OnInit {

    promoCode: string = '';
    code: string = '';

    constructor(
        private bsModalRef: BsModalRef,
        private signupService: SignupService,
        private toastService: ToastrService
    ) { }

    ngOnInit() {
    }

    onSubmit() {
        const submitData = {
            promo: this.promoCode,
            code: this.code
        }
        this.signupService.validationPromo(submitData).subscribe(res => {
            if (res.isValid) {
                this.signupService.setBillingSource(res.billing);
            } else {
                this.toastService.error('This promo code is invalid.');    
            }
            this.onClose();
        })
    }

    onClose() {
        this.bsModalRef.hide();
    }
}
