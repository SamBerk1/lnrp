import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from '../../../environments/environment';

import { ReleaseConstants } from '../../utils';

import { AuthService } from '../auth/auth.service';
import { ReleaseService } from '../release/release.service';
import { GenreService } from '../../services/genre.service';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'advanced-search',
    templateUrl: './advanced.component.html'
})

export class AdvancedSearchComponent implements OnInit {

    public release_count: Number = 0;

    public genres: Object[];
    public subgenres: Object[] = [];

    public searchText: string;
    public searches: string[] = [];

    public decades: Object[];

    public versions: Object[];

    public bpm: Object[];

    public letters: Object[];

    public dates: Object[];
    public years: number[];
    public searchByYear = false;
    public searchByYearForm =  new FormControl(false, []);
    public yearRange = {
        minYear: null,
        maxYear: null,
    };

    public throwback: Boolean = false;
    public remix: Boolean = false;

    constructor(
        private releaseService: ReleaseService,
        private authService: AuthService,
        private genreService: GenreService,
        private _router: Router
    ) {
        this.decades = ReleaseConstants.decades;
        this.versions = ReleaseConstants.versions;
        this.bpm = ReleaseConstants.bpm;
        this.letters = ReleaseConstants.letters;
        this.dates = ReleaseConstants.dates;
    }

    ngOnInit() {
        this.populateYears();
        this.searchByYearForm.valueChanges.subscribe(val => {
            this.searchByYear = val;
            if (this.searchByYear) {
                this.clearSelected('decades');
            } else {
                this.clearSelected('years');
            }
        });

        this.genreService.loadGenres();
        this.releaseService.loadLabels();

        this.genreService.getParentGenresByReleaseCount()
            .then((g) => {
                g.forEach((itm) => {
                    itm.selected = false;
                });

                this.genres = g;

                this.genres[0]['selected'] = true;
            });

        this.genreService.getAllSubgenres('releases')
            .then((g) => {
                g.forEach((itm) => {
                    itm.subgenres.unshift({
                        id: 0,
                        name: 'All Releases',
                        releases: itm.releases
                    });

                    itm.subgenres.forEach((sg) => {
                        sg.selected = false;
                    });

                    if (itm.id === 9) {
                        itm.selected = true;
                    } else
                        itm.selected = false;
                });

                this.subgenres = g;

                this.loadFilterParams();
            });

        this.releaseService.getTotalReleaseCount()
            .then((count) => {
                this.release_count = count.total;
            });

        if (this.releaseService.isFilterModified) {
            this.parseFilterParams(
                this.releaseService.getFilterParams()
            );
        }
    }

    public populateYears(): void {
        let year = new Date().getFullYear();;
        const minYear = 1970;
        this.years = [];
        while(year >= minYear) {
            this.years.push(year--);
        }
    }

    get labels(): Object[] {
        return this.releaseService.labels;
    }

    addSearch = (evt: any) => {
        console.log(evt);
        // Handles if the 'Add to Search' button is clicked, or if the enter key is pressed in the input field
        if ((evt.key !== undefined && evt.key === 'Enter') || evt.type == 'click') {

            // don't let user add a null or completely empty string with just spaces
            if (!this.searchText || !this.searchText.trim()) {
                this.searchText = '';
                return;
            }

            this.searches.push(this.searchText);
            this.searchText = '';
        }
    }

    removeSearch = (key: number) => {
        this.searches.splice(key, 1);
    }

    selectGenre = (id: Number) => {
        this.genres.forEach((g) => {
            g['selected'] = false;

            if (g['id'] === id) {
                g['selected'] = true;
            }
        });

        this.subgenres.forEach((sg) => {
            sg['selected'] = false;

            if (sg['id'] === id) {
                sg['selected'] = true;
            }
        });
    }

    selectDate = (id: string) => {
        this.dates.forEach((d) => {
            d['selected'] = false;

            if (d['id'] === id) {
                d['selected'] = true;
            }
        });
    }

    clearSelected = (type: string) => {
        switch (type) {
            case 'search':
                this.searches = [];
                break;

            case 'decade':
                this.decades.forEach((d) => {
                    d['selected'] = false;
                });
                break;
            
            case 'years':
                this.yearRange.maxYear = null;
                this.yearRange.minYear = null;
                break;

            case 'version':
                this.versions.forEach((v) => {
                    v['selected'] = false;
                });
                break;

            case 'bpm':
                this.bpm.forEach((b) => {
                    b['selected'] = false;
                });
                break;

            case 'letter':
                this.letters.forEach((l) => {
                    l['selected'] = false;
                });
                break;

            case 'date':
                this.dates.forEach((d) => {
                    d['selected'] = false;
                });
                break;
        }
    }

    submitSearch = () => {
        this.releaseService.setFilter(this.compileSearchFilter());
        this.releaseService.page_title = 'Search Results';

        this._router.navigate(['releases']);
    }

    private loadFilterParams = () => {
        if (this.releaseService.isFilterModified) {
            this.parseFilterParams(
                this.releaseService.getFilterParams()
            );
        }
    }

    private compileSearchFilter = () => {
        let filter: Object = {
            genre_id: [],
            subgenre_id: [],
            label: [],
            letter: [],
            decade: [],
            version: [],
            bpm: [],
            date: [],
            minYear: this.yearRange.minYear,
            maxYear: this.yearRange.maxYear,
            throwback: null,
            remix: null,
            search: [],
            sort: 'date'
        };

        // Collect all search terms
        filter['search'] = this.searches;

        // Collect all selected decades
        this.decades.forEach((d) => {
            if (d['selected']) {
                filter['decade'].push(d['year']);
            }
        });

        // Collect all selected versions
        this.versions.forEach((v) => {
            if (v['selected']) {
                filter['version'].push(v['id']);
            }
        });

        // Collect all selected bpm types
        this.bpm.forEach((b) => {
            if (b['selected']) {
                filter['bpm'].push(b['id']);
            }
        });

        // Collect all selected labels
        this.releaseService.labels.forEach((l) => {
            if (l['selected']) {
                filter['label'].push(l['name']);
            }
        });

        // Collect all selected letters
        this.letters.forEach((l) => {
            if (l['selected']) {
                filter['letter'].push(l['letter']);
            }
        });

        // Collect all selected date values
        this.dates.forEach((d) => {
            if (d['selected']) {
                filter['date'].push(d['id']);
            }
        });

        // Collect throwback and remix values
        filter['throwback'] = this.throwback ? true : null;
        filter['remix'] = this.remix ? true : null;

        // Collect all selected genre and subgenre values
        this.subgenres.forEach((g) => {
            g['subgenres'].forEach((sg) => {
                if (sg['selected']) {
                    if (sg['id'] !== 0) {
                        filter['subgenre_id'].push(sg['id']);
                    } else {
                        filter['genre_id'].push(g['id']);
                    }
                }
            });
        });

        return filter;
    }

    private parseFilterParams = (params: Object) => {
        // Set genre id values
        if (params['genre_id'].length > 0) {
            this.subgenres.forEach((g) => {
                if (params['genre_id'].indexOf(g['id']) !== -1) {
                    g['subgenres'].forEach((sg) => {
                        if (sg['id'] === 0) {
                            sg['selected'] = true;
                        }
                    });
                }
            });
        }

        // Set subgenre id values
        if (params['subgenre_id'].length > 0) {
            this.subgenres.forEach((g) => {
                g['subgenres'].forEach((sg) => {
                    if (params['subgenre_id'].indexOf(sg['id']) !== -1) {
                        sg['selected'] = true;
                    }
                });
            });
        }

        // Set decade values
        if (params['decade'].length > 0) {
            this.decades.forEach((d) => {
                if (params['decade'].indexOf(d['year']) !== -1) {
                    d['selected'] = true;
                }
            });
        }

        // Set version values
        if (params['version'].length > 0) {
            this.versions.forEach((v) => {
                if (params['version'].indexOf(v['id']) !== -1) {
                    v['selected'] = true;
                }
            });
        }

        // Set BPM values
        if (params['bpm'].length > 0) {
            this.bpm.forEach((b) => {
                if (params['bpm'].indexOf(b['id']) !== -1) {
                    b['selected'] = true;
                }
            });
        }

        // Set letter values
        if (params['letter'].length > 0) {
            this.letters.forEach((l) => {
                if (params['letter'].indexOf(l['letter']) !== -1) {
                    l['selected'] = true;
                }
            });
        }

        // Set date values
        if (params['date'].length > 0) {
            this.dates.forEach((d) => {
                if (params['date'].indexOf(d['id']) !== -1) {
                    d['selected'] = true;
                }
            });
        }

        // Set throwback value
        if (params['throwback'] !== null) {
            this.throwback = params['throwback'];
        }

        // Set remix value
        if (params['remix'] !== null) {
            this.remix = params['remix'];
        }

        if (params['minYear'] !== null) {
            this.yearRange.minYear = params['minYear'];
        }

        if (params['maxYear'] !== null) {
            this.yearRange.maxYear = params['maxYear'];
        }

        // Set search values
        if (params['search'].length > 0) {
            this.searches = params['search'];
        }
    }
}
