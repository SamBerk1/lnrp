import { Component, ViewChild, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeviceDetectorService } from 'ngx-device-detector';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';

import { environment } from '../../../environments/environment';

import { AuthService } from '../auth/auth.service';
import { FrontpageService } from './frontpage.service';
import { WavePlayerService } from '../waveplayer/waveplayer.service';
import { ReleaseService } from '../release/release.service';
import { GeneralService } from '../../services';
import { DetailFormComponent } from '../detail-form/detail-form.component';

import * as $ from 'jquery';

@Component({
    selector: 'front-page',
    templateUrl: './frontpage.component.html'
})

export class FrontpageComponent implements OnInit, AfterViewInit {
    @ViewChild('frontpageHero') el_frontpage_hero: ElementRef;

    detailFromModalRef: BsModalRef;

    contactForm: FormGroup;

    _sanitizer: any;

    token: any;

    total_releases: any;

    test_image: any;

    hero_image_map: Array<Number> = [];

    primary_hero_images: any;
    full_hero_images: any;
    top_download_images: any;
    most_downloaded: any = {};

    currently_playing_now: any;
    currently_playing: any;

    always_new_url: string = `url('${environment.apiURL}/images/bg/front-new')`;

    features: any[];

    genres = [
        {
            id: 1,
            value: 'DJ Edits',
        },
        {
            id: 3,
            value: 'Hip-Hop',
        },
        {
            id: 9,
            value: 'Remix',
        },
        {
            id: 8,
            value: 'Rap',
        },
        {
            id: 2,
            value: 'EDM',
        },
        {
            id: 6,
            value: 'Pop',
        },
        {
            id: 7,
            value: 'R&B',
        },
        {
            id: 13,
            value: 'Country',
        },
        {
            id: 16,
            value: 'Reggae',
        },
        {
            id: 17,
            value: 'Seasonal',
        },
        {
            id: 25,
            value: 'Indie',
        },
        {
            id: 12,
            value: 'Various',
        },
    ];

    labels = ['Atlantic', 'Spinnin', 'Ultra', 'Republic', 'RCA', 'Epic', 'Sony', 'Capitol', 'Interscope', 'Jive', 'Def-Jam', 'Columbia'];

    isDesktop: boolean = false;

    constructor(
        private authService: AuthService,
        private frontpageService: FrontpageService,
        private waveplayerService: WavePlayerService,
        private releaseService: ReleaseService,
        private generalService: GeneralService,
        private _router: Router,
        private modalService: BsModalService,
        private toastService: ToastrService,
        private deviceService: DeviceDetectorService,
        private fb: FormBuilder,
    ) {
        this.features = this.frontpageService.getFeatureSet();
        this.contactForm = this.fb.group({
            'name': [null, Validators.required],
            'email' : [null, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)],
            'phone' : '',
            'message' : [null, Validators.required]
        });

        this.isDesktop = this.deviceService.isDesktop();

        this.generalService.toggleDesktop$.subscribe(isDesktop => {
            this.isDesktop = isDesktop;
        })
    }

    ngOnInit() {
        let context = this;

        this.primary_hero_images = [];
        this.full_hero_images = [];

        this.frontpageService.getPrimaryHeroImages()
            .then((release_images) => {
                this.primary_hero_images = release_images;

                let context = this;

                $.each(release_images, function(k, v) {
                    context.primary_hero_images[k]['image'] = `${environment.apiURL}/images/src/${v.id}/200/200/30`;

                    context.hero_image_map.push(v.id);
                });
            });

        this.frontpageService.getTotalReleaseCount()
            .then((response) => {
                this.total_releases = response.total;
            });

        this.frontpageService.getFullHeroImages()
            .then((response) => {
                this.full_hero_images = response;

                setInterval(this.swapReleaseImage, 1500, this);
            });

        this.frontpageService.getTopDownloadImages()
            .then((response) => {
                let topImage = response.shift();

                this.top_download_images = response;

                let context = this;

                $.each(this.top_download_images, function(k, v) {
                    context.top_download_images[k]['image'] = `${environment.apiURL}/images/src/${v.id}/140/140/50`;
                });
            });

        this.frontpageService.getTopDownloadedRelease()
            .then((response) => {
                this.most_downloaded = response;

                this.most_downloaded['image'] = `${environment.apiURL}/images/src/${this.most_downloaded['id']}/350/350`;

                this.releaseService.getReleaseDetail(this.most_downloaded.id).then((res) => {
                    this.most_downloaded = res;
                    this.most_downloaded['image'] = `${environment.apiURL}/images/src/${this.most_downloaded['id']}/350/350`;
                }, (err) => {
                    console.log('Get most detail error: ', err);
                });
            });

        this.frontpageService.getCurrentlyPlaying()
            .then((response) => {
                this.currently_playing_now = response.shift();
                this.currently_playing_now['image'] = `${environment.apiURL}/images/src/${this.currently_playing_now.id}/250/250`;

                this.releaseService.getReleaseDetail(this.currently_playing_now.id).then((res) => {
                    this.currently_playing_now = res;
                    this.currently_playing_now['image'] = `${environment.apiURL}/images/src/${this.currently_playing_now.id}/250/250`;
                }, (err) => {
                    console.log('Get current detail error: ', err);
                });

                this.currently_playing = response;

                let promises = [];
                this.currently_playing.forEach(item => {
                    const promise = this.releaseService.getReleaseDetail(item.id);
                    promises.push(promise);
                });
                Promise.all(promises).then((result) => {
                    this.currently_playing = result;
                });
            });
    }

    ngAfterViewInit() {
    }

    get isLoggedIn(): Boolean {
        return this.authService.isLoggedIn;
    }

    swapReleaseImage(context) {
        let image_to_swap_id: any,
            image_to_swap: any,

            new_image_id: any,
            new_image: any,

            old_image_map_id: any,

            release_wrapper: any,
            element_to_swap: any,

            new_release_div: any;

        // Pick an image to swap out
        image_to_swap_id = Math.floor(Math.random() * (context.primary_hero_images.length - 1));
        image_to_swap = context.primary_hero_images[image_to_swap_id];

        // Grab an image that's not already in the map
        do {
            if (context.full_hero_images.length === 0) continue;

            new_image_id = Math.floor(Math.random() * (context.full_hero_images.length - 1));
            new_image = context.full_hero_images[new_image_id];
        } while (context.hero_image_map.indexOf(new_image.id) !== -1);

        // Now that we have a new image, replace it in the map first
        old_image_map_id = context.hero_image_map.indexOf(image_to_swap.id);
        context.hero_image_map[old_image_map_id] = new_image.id;

        // Now replace the element itself
        release_wrapper = $('.releases-container .release-image', $(context.el_frontpage_hero.nativeElement)).get(image_to_swap_id);
        element_to_swap = $('.release', release_wrapper);

        new_release_div = $('<div/>').addClass('release bottom').css({
            'background-image': `url('${environment.apiURL}/images/src/${new_image.id}/200/200/30')`
        });

        $(release_wrapper).append(new_release_div);

        $(element_to_swap).fadeOut(1500, function() {
            new_release_div.removeClass('bottom');
            $(this).remove();
        });
    }

    requestRelease(evt: Event, release) {
        evt.preventDefault();

        this.waveplayerService.requestRelease(release.id);
        if (!release.image) {
            release.image = environment.apiURL + '/images/src/' + release.id;
        }
        this.waveplayerService.requestDetail(release);
    }

    setGenre = (id: Number, genre: string) => {
        this.releaseService.setGenre(id, genre);
        this.frontpageClickCleanup();
    }

    setLabel = (label: string) => {
        if (label === 'Def-Jam') {
            this.releaseService.setLabel('Def Jam');    
        } else {
            this.releaseService.setLabel(label);
        }
        this.frontpageClickCleanup();
    }

    openLogin = (evt: any) => {
        evt.preventDefault();

        this.authService.openLoginModal();
    }

    openDetail = (item) => {
        if (item.versions) {
            this.detailFromModalRef = this.modalService.show(DetailFormComponent, { class: 'detail-form' });
            this.detailFromModalRef.content.id = item.id;
            if (!item.image) {
                item.image = `${environment.apiURL}/images/src/${item.id}`;
            }
            this.detailFromModalRef.content.img_src = item.image;
            this.detailFromModalRef.content.release_detail = item;
        }
    }

    private frontpageClickCleanup = () => {
        if (this._router.routerState.snapshot.url === '/releases') {
            this.releaseService.requestReleasesBroadcast();
        } else {
            this._router.navigate(['releases']);
        }
    };

    get showError(): Boolean {
        return (this.contactForm.dirty || this.contactForm.touched) &&
               (
                   this.contactForm.controls['name'].errors !== null && this.contactForm.controls['name'].errors['required'] ||
                   this.contactForm.controls['email'].errors !== null && this.contactForm.controls['email'].errors['pattern'] ||
                   this.contactForm.controls['email'].errors !== null && this.contactForm.controls['email'].errors['required'] ||
                   this.contactForm.controls['message'].errors !== null && this.contactForm.controls['message'].errors['required']
               );
    }

    fieldModified(name): Boolean {
        return this.contactForm.controls[name].touched && this.contactForm.controls[name].dirty
    }

    submitForm = (form: any) => {
        form.preventDefault();

        this.generalService.sendContactForm(this.contactForm.value)
            .subscribe(
                (success) => {
                    this.toastService.success('Thanks! We will contact you as soon as possible.');
                    this.contactForm.reset();
                },
                (err) => {
                    this.toastService.error(err.message);
                }
            );
    }
}
