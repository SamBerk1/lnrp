import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { HTTPInterceptor } from '../../services/interceptor.service';

import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class FrontpageService {
    constructor(
        // private http: Http,
        private newHttp: HTTPInterceptor
    ) { }

    private handleError(error: any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    private extractJSON(res: Response) {
        let json = res.json();
        return json._body || '';
    }

    public getPrimaryHeroImages() {
        return this.newHttp.get(environment.apiURL + '/images/hero')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    public getTotalReleaseCount() {
        return this.newHttp.get(environment.apiURL + '/releases/count')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    public getFullHeroImages() {
        return this.newHttp.get(environment.apiURL + '/images/hero/full')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    public getTopDownloadImages() {
        return this.newHttp.get(environment.apiURL + '/images/top')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    public getCurrentlyPlaying() {
        return this.newHttp.get(environment.apiURL + '/releases/current/10')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    getTopDownloadedRelease = () => {
        return this.newHttp.get(environment.apiURL + '/release/top')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }
    public getFeatureSet(): any[] {
        return [
            {
                path: "",
                img: 'assets/img/features-curated.svg',
                label: 'Curated by Professional DJs',
            },
            {
                path: "",
                img: 'assets/img/features-dropbox.svg',
                label: 'Dropbox Connected',
            },
            {
                path: "",
                img: 'assets/img/features-crateq.svg',
                label: 'crateQ Desktop Downloader',
            },
            {
                path: "",
                img: 'assets/img/features-downloads.svg',
                label: 'Convenient ZIP Downloads',
            },
            {
                path: "",
                img: 'assets/img/features-variety.svg',
                label: 'High-Quality Audio',
            },
            {
                path: "",
                img: 'assets/img/features-selection.svg',
                label: 'Rare & Hard-to-Find Selections',
            },
            {
                path: "",
                img: 'assets/img/features-star.svg',
                label: 'Certified Instrumentals',
            },
            {
                path: "",
                img: 'assets/img/features-search.svg',
                label: 'Pinpoint Search',
            },
        ];
    }
}
