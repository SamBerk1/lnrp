import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { HTTPInterceptor } from '../../services/interceptor.service';

import { environment } from '../../../environments/environment';

import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

@Injectable()
export class WavePlayerService {

    private releaseSource = new Subject<Number>();
    private versionSource = new Subject<Number>();
    private releaseDetail = new Subject<any>();

    release$ = this.releaseSource.asObservable();
    version$ = this.versionSource.asObservable();
    details$ = this.releaseDetail.asObservable();

    constructor(
        private newHttp: HTTPInterceptor
    ) {}

    requestVersion(version: Number) {
        this.versionSource.next(version);
    }

    requestRelease(release: Number) {
        this.releaseSource.next(release);
    }

    requestDetail(detail: any) {
        this.releaseDetail.next(detail);
    }

    private handleError(error: any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    private extractJSON(res: Response) {
        let json = res.json();
        return json._body || '';
    }

    public loadReleaseInfo(release) {
        return this.newHttp.get(environment.apiURL + '/release/info/' + release)
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    public loadVersionInfo(version) {
        return this.newHttp.get(environment.apiURL + '/version/info/' + version)
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }
}
