import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Observable, Subscription } from 'rxjs/Rx';

import { Howl } from 'howler';

import { environment } from '../../../environments/environment';

import { AudioWaveformModule } from './waveform.module';

import { WavePlayerService } from './waveplayer.service';
import { AuthService } from '../auth/auth.service';
import { DetailFormComponent } from '../detail-form/detail-form.component';

@Component({
    selector: 'waveplayer',
    templateUrl: './waveplayer.component.html',
})

export class WavePlayerComponent {
    @ViewChild('musicPlayer')
    private el_player: ElementRef;

    detailFromModalRef: BsModalRef;

    player1: Howl;
    isLoaded: boolean = false;
    song_info: Object = {};

    isPlaying: boolean = false;
    isSeeking: boolean = false;
    muted: boolean = false;
    volume: number = 40;

    releaseDetails: any = {};

    playingInfo: Object = {
        title : '',
        artist : '',
        current_time : 0,
        total_time: 0
    };

    display_message: number = 0;
    waveImgUrl: string;

    private timer;
    private timeSub: Subscription;

    constructor(
        private waveplayerService: WavePlayerService,
        private authService: AuthService,
        private modalService: BsModalService
    ) {

        waveplayerService.release$.subscribe(
            release => {
                if (this.isPlaying) {
                    this.stopPlayer();
                }

                this.loadReleaseInfo(release);
            }
        );

        waveplayerService.version$.subscribe(
            version => {
                if (this.isPlaying) {
                    this.stopPlayer();
                }

                this.loadVersionInfo(version);
            }
        );

        waveplayerService.details$.subscribe(
            details => {
                this.releaseDetails = details;
            }
        );

        let this_ = this;
        this.timer = Observable.timer(50, 250);
    }

    audioPlay = (type: String, id: Number) => {
        let context = this;
        let url = '';

        if (this.player1) {
            this.player1.unload();
        }

        if (this.authService.isLoggedIn) {
            url = environment.apiURL + '/' + type + '/play/' + id;
        } else {
            let newId = 100000000 - (id.valueOf() * (new Date().getUTCDay() + 1));
            url = environment.apiURL + '/' + type + '/play/' + newId;
        }

        this.isLoaded = true;

        this.player1 = new Howl({
            src: [url],
            html5: true,
            autoplay: true,
            mute: context.muted,
            volume: this.volume / 100,
            loop: true
        });

        this.player1.on('end', () => {
            // console.log('---play finished---');
            this.playingInfo['current_time'] = 0;
        });

        this.player1.on('load', () => {
            // console.log('++load++');
        });

        this.player1.on('loaderror', (sID, errCode) => {
            // console.log('--load error--', sID, errCode);
        });

        this.player1.on('playerror', (sID, errCode) => {
            // console.log('--play error--', sID, errCode);
        });

        this.player1.on('play', () => {
            // console.log('--play--');
        });

        this.audioPlayer();

        this.timeSub = this.timer.subscribe(
            (interval) => {
                if (this.isPlaying && !this.isSeeking) {
                    this.playingInfo['current_time'] = this.player1.seek();
                }
            }
        );
    }

    audioPlayer = () => {
        setTimeout(() => {
            if (!window['iOS']) {
                this.startPlayer();
            }
        }, 50);
    }

    startPlayer = () => {
        this.player1.play();
        this.isPlaying = true;
    }

    pausePlayer = () => {
        this.player1.pause();
        this.isPlaying = false;
    }

    stopPlayer = () => {
        this.player1.stop();
        this.isPlaying = false;
    }

    loadReleaseInfo = (release: Number) => {

        this.waveplayerService.loadReleaseInfo(release)
            .then((info) => {
                this.isLoaded = true;

                this.song_info = info;
                this.updatePlayerInfo();
                this.audioPlay('release', release);
            });
    }

    loadVersionInfo = (version: Number) => {
        this.waveplayerService.loadVersionInfo(version)
            .then((info) => {
                this.isLoaded = true;

                this.song_info = info;
                this.updatePlayerInfo();
                this.audioPlay('version', version);
            });
    }

    private updatePlayhead = (context) => {
        context.playingInfo['current_time']++;
    }

    private updatePlayerInfo = () => {
        this.playingInfo['title'] = this.song_info['title'] + ' - ' + this.song_info['type'];
        this.playingInfo['artist'] = this.song_info['artist'];
        this.playingInfo['current_time'] = 0;
        this.playingInfo['total_time'] = this.song_info['time'];
        this.waveImgUrl = this.song_info['wave'];
    }

    clickPlayPause = () => {
        this.display_message++;

        if (this.isPlaying) {
            this.pausePlayer();

        } else {
            this.startPlayer();
        }
    }

    changeVolume = (value: number) => {
        this.volume = value;
        this.player1.volume(value / 100);
    }

    clickMute = () => {
        this.muted = !this.muted;

        this.player1.volume(this.muted ? 0 : this.volume);
    }

    clickClose = () => {
        this.stopPlayer();
        this.player1.unload();
        this.isLoaded = false;
        this.timeSub.unsubscribe();
    }

    getPlayheadPosition = (): string => {
        return ((this.playingInfo['current_time'] / this.playingInfo['total_time']) * 100) + '%';
    }

    getTimelineFill = (): string => {
        return (this.playingInfo['current_time'] / this.playingInfo['total_time']) * 100 + '%';
    }

    seekingTrack = () => {
        this.isSeeking = true;
    }

    seekTrack = (percent: number) => {
        let seekTo = this.playingInfo['total_time'] * (percent / 100);

        this.isSeeking = false;
        this.player1.seek(seekTo);
    }

    openDetail = () => {
        this.detailFromModalRef = this.modalService.show(DetailFormComponent, { class: 'detail-form' });
        this.detailFromModalRef.content.id = this.releaseDetails.id;
        this.detailFromModalRef.content.img_src = this.releaseDetails.image;
        this.detailFromModalRef.content.release_detail = this.releaseDetails;
    }
}
