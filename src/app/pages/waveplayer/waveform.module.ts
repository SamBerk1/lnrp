import {
    Component,
    NgModule,
    Input,
    Output,
    ViewChild,
    HostListener,
    ElementRef,
    EventEmitter,
    OnInit,
    SimpleChanges
} from '@angular/core';
import { CommonModule } from '@angular/common';

import { Observable } from 'rxjs/Rx';

@Component({
    selector: 'waveform',
    template: `
        <div #timelineContainer class='waveform-container'>
            <div #playhead class='playhead' [style.left]='playheadPosition'></div>
            <div #timelinePlayed class='track-played' [style.width]='timelineFill'></div>
        </div>
    `
})
export class AudioWaveform implements OnInit {
    @ViewChild('playhead')
    private playhead: ElementRef;

    @ViewChild('timelinePlayed')
    private timelinePlayed: ElementRef;

    @ViewChild('timelineContainer')
    private timelineContainer: ElementRef;

    @Input('waveimageSrc')
    private waveimageUrl: string;

    @Input('playheadPosition')
    public playheadPosition: string;

    @Input('timelineFill')
    public timelineFill: string;

    @Output()
    onSeeking = new EventEmitter();

    @Output()
    onSeek = new EventEmitter();

    private mousedrag;
    private mouseup = new EventEmitter();
    private mousedown = new EventEmitter();
    private mousemove = new EventEmitter();

    private position: number = 0;

    @HostListener('mousedown', ['$event'])
    onMouseDown = (evt: any) => {
        this.mousedown.next(evt);
        this.onSeeking.emit();
    }

    @HostListener('mouseup', ['$event'])
    onMouseUp = (evt: any) => {
        this.mouseup.next(evt);
        this.onSeek.emit(this.position);
    }

    @HostListener('mousemove', ['$event'])
    onMousemove = (evt: any) => {
        this.mousemove.emit(evt);
    }

    constructor() {}

    ngOnInit() {
        // Find the center of the playhead
        let playheadRect = this.playhead.nativeElement.getBoundingClientRect();
        let playheadCenter = playheadRect.right - ((playheadRect.right - playheadRect.left) / 2);

        // Determine the bounds of the timeline
        let timelineWidth = this.timelineContainer.nativeElement.getBoundingClientRect().width;
        let timelineLeft = this.timelineContainer.nativeElement.getBoundingClientRect().left;
        let timelineRight = this.timelineContainer.nativeElement.getBoundingClientRect().right;

        //Setting Styles
        this.timelineContainer.nativeElement.style.backgroundImage = `url(${this.waveimageUrl})`;    

        this.mousedown
            .switchMap(event => {
                return Observable.create((observer) => {
                    observer._next({
                        left: playheadCenter
                    });
                });
            })
            .flatMap((imageOffset) => {
                return this.mousemove.flatMap((moveEvent) => {
                    return Observable.create((observer) => {
                        if (moveEvent['target'] !== this.playhead.nativeElement) {
                            observer._next({
                                left: (moveEvent['offsetX'] / timelineWidth) * 100
                            });
                        }
                    });
                }).takeUntil(this.mouseup);
            })
            .subscribe({
                next: pos => {
                    this.playhead.nativeElement.style.left = pos['left'] + '%';
                    this.position = pos['left'];
                }
            });

            this.mouseup.subscribe({
                next: moveEvent => {
                    if (moveEvent['target'] !== this.playhead.nativeElement) {
                        let pos = (moveEvent['offsetX'] / timelineWidth) * 100;
                        this.playhead.nativeElement.style.left = pos + '%';
                        this.position = pos;
                    }
                }
            });
    }

    ngOnChanges(changes: SimpleChanges) {
        // console.log('Myth : ', changes);
        if (changes.waveimageUrl !== undefined) {
            this.timelineContainer.nativeElement.style.backgroundImage = `url(${changes.waveimageUrl.currentValue})`;        
        }
    }
}

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        AudioWaveform,
    ],
    exports: [
        AudioWaveform,
    ],
})
export class AudioWaveformModule {

}
