import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ReCaptchaComponent } from 'angular2-recaptcha/lib/captcha.component';

import { GeneralService } from '../../services';

@Component({
    selector: 'contact-us',
    templateUrl: './contact.component.html',
    encapsulation: ViewEncapsulation.None
})

export class ContactComponent implements OnInit {
    @ViewChild(ReCaptchaComponent)
    private captcha:ReCaptchaComponent;

    contactForm: FormGroup;

    constructor(
        private generalService: GeneralService,
        private fb: FormBuilder,
        private toastService: ToastrService
    ) {
        this.contactForm = fb.group({
            'purpose' : 'General',
            'name': [null, Validators.required],
            'email' : [null, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)],
            'phone' : '',
            'message' : [null, Validators.required]
        });
    }

    ngOnInit() {
    }

    get captchaComplete(): Boolean {
        return this.captcha.getResponse() !== null && this.captcha.getResponse().length > 0;
    }

    get showError(): Boolean {
        return (this.contactForm.dirty || this.contactForm.touched) &&
               (
                   !this.captchaComplete ||
                   this.contactForm.controls['name'].errors !== null && this.contactForm.controls['name'].errors['required'] ||
                   this.contactForm.controls['email'].errors !== null && this.contactForm.controls['email'].errors['pattern'] ||
                   this.contactForm.controls['email'].errors !== null && this.contactForm.controls['email'].errors['required'] ||
                   this.contactForm.controls['message'].errors !== null && this.contactForm.controls['message'].errors['required']
               );
    }

    submitForm = (form: any) => {
        form.preventDefault();

        this.generalService.sendContactForm(this.contactForm.value)
            .subscribe(
                (success) => {
                    this.toastService.success('Thanks! We will contact you as soon as possible.');
                    this.contactForm.reset();
                },
                (err) => {
                    this.toastService.error(err.message);
                }
            );
    }
}
