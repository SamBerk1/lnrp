import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { environment } from '../../../environments/environment';

import { AuthService } from './auth.service';

@Component({
    selector: 'login',
    templateUrl: './auth.component.html'
})
export class AuthComponent {
    @ViewChild('childModal')
    public childModal: ModalDirective;

    @ViewChild('forgotPasswordModal')
    public forgotPasswordModal: ModalDirective;

    @ViewChild('renewAccountModal')
    public renewAccountModal: ModalDirective;

    isChecked: boolean = false;

    constructor(
        private authService: AuthService,
        private toastService: ToastrService
    ) {
        this.authService.loginModal$
            .subscribe(
                () => {
                    this.showModal();
                }
            );

        this.authService.renewModal$
            .subscribe(
                () => {
                    this.showRenewModal();
                }
            );
    }

    showModal = () => {
        this.childModal.show();
    }

    hideModal = () => {
        this.childModal.hide();
    }

    showPasswordModal = () => {
        this.forgotPasswordModal.show();
    }

    hidePasswordModal = () => {
        this.forgotPasswordModal.hide();
    }

    showRenewModal = () => {
        this.renewAccountModal.show();
    }

    hideRenewModal = () => {
        this.renewAccountModal.hide();
    }

    openForgotPasswordModal = () => {
        this.hideModal();
        this.hideRenewModal();
        this.showPasswordModal();
    }

    onSubmit = (form: any) => {
        form.ip_address = localStorage.getItem('lnrp-ip');
        this.authService.doLogin(form)
            .subscribe(
                (result) => {
                    this.hideModal();
                }
            );
    }

    submitPasswordForm = (form: any) => {
        this.authService.sendResetLink(form)
            .subscribe(
                (result) => {
                    this.showNotification('Your resent link has been sent!');
                    this.hidePasswordModal();
                }
            );
    }

    submitRenewForm = (form: any) => {
        form.isChecked = false;
        form.ip_address = localStorage.getItem('lnrp-ip');
        this.authService.doLogin(form)
        .subscribe(
            (result) => {
                this.hideModal();
            }
        );
    }

    showNotification(message: string) {
        this.toastService.success(message);
    }
}
