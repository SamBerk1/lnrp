import { Injectable, transition } from '@angular/core';
import { Response } from '@angular/http';
import { ToastrService } from 'ngx-toastr';

import { environment } from '../../../environments/environment';
import { JWTObject } from '../../services/jwtobject';
import { HTTPInterceptor } from '../../services/interceptor.service';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthService {
    private loginModalSource = new Subject<Number>();
    private loginAnnounceSource = new Subject<Number>();
    private logoutAnnounceSource = new Subject<Number>();
    private renewModalSource = new Subject<Number>();

    private _loggedIn: Boolean = false;

    loginModal$ = this.loginModalSource.asObservable();
    loginAnnounce$ = this.loginAnnounceSource.asObservable();
    logoutAnnounce$ = this.logoutAnnounceSource.asObservable();
    renewModal$ = this.renewModalSource.asObservable();

    constructor(
        private newHttp: HTTPInterceptor,
        private toastService: ToastrService
    ) {}

    get isLoggedIn(): Boolean {
        return this._loggedIn;
    }

    get user(): Object {
        const jwt: JWTObject = this.newHttp.jwt;

        return jwt['payload'];
    }

    set isLoggedIn(loggedIn: Boolean) {
        this._loggedIn = loggedIn;
    }

    openLoginModal = () => {
        this.loginModalSource.next();
    }

    openRenewModal = () => {
        this.renewModalSource.next();
    }

    registerLogout = () => {
        this.doLogout();

        this.renewModalSource.next();
    }

    doLogin = (form: Object) => {
        return this.newHttp
            .post(`${environment.apiURL}/user/login`, form)
            .catch((error) => {
                let errMsg: string;
                let value: string;
                let code: string;

                if (error instanceof Response) {
                    const body = error.json() || '';
                    errMsg = `${body.error || ''}`;
                    value = `${body.value || ''}`;
                    if (value === 'disabled') {
                        code = `${body.code.code || ''}`;
                        if (body.code.user) {
                            const userId = body.code.user;
                            localStorage.setItem('lnrp-user', userId);
                        }
                    }
                } else {
                    errMsg = error.message ? error.message : error.toString();
                }

                if (value && value === 'disabled') {
                    this.toastService.error(`<h4>Your account is expired. <a href='/renew-account/${code}'>
                        Click here</a> to redirect you to the renewal page. If you feel this is an error, please contact support <a
                        href='/contact'>here</a>.</h4>`, '', {
                        enableHtml: true
                    });
                } else if (value && (value === 'review' || value === 'deactivated')) {
                    this.toastService.error(`<h4>You're still within the 24-48 hour period where your account is being reviewed.
                         Please be patient, as we check every account manually for security purposes. If after 48 hours
                         you still haven't been approved, please contact support <a href='/contact'>here</a>.</h4>`, '', {
                        enableHtml: true
                    });
                } else {
                    this.showNotification(errMsg);
                }

                return Observable.throw(errMsg);
            })
            .do((res) => {
                this._loggedIn = true;
                this.loginAnnounceSource.next();
            });
    }

    doLogout = () => {
        this._loggedIn = false;

        this.newHttp.jwt = new JWTObject();

        localStorage.removeItem('_t');
        localStorage.removeItem('lnrp-user');
        localStorage.removeItem('lnrp-crateqs');

        this.logoutAnnounceSource.next();
    }

    processLogin = (loggedIn: Boolean) => {
        this._loggedIn = loggedIn;
    }

    refreshUserToken = () => {
        return this.newHttp.get(`${environment.apiURL}/user/refresh`);
    }

    refreshStoredUser = () => {
        this.newHttp.refreshStoredUser(this.user);
    }

    sendResetLink = (form: any) => {
        return this.newHttp
            .post(`${environment.apiURL}/user/reset`, form)
            .catch((error) => {
                let errMsg: string;

                if (error instanceof Response) {
                    const body = error.json() || '';
                    errMsg = `${body.error || ''}`;

                } else {
                    errMsg = error.message ? error.message : error.toString();
                }

                this.showNotification(errMsg);

                return Observable.throw(errMsg);
            });
    }

    sendRenewLink = (form: any) => {
        return this.newHttp
            .post(`${environment.apiURL}/user/renew`, form)
            .catch((error) => {
                let errMsg: string;

                if (error instanceof Response) {
                    const body = error.json() || '';
                    errMsg = `${body.error || ''}`;

                } else {
                    errMsg = error.message ? error.message : error.toString();
                }

                this.showNotification(errMsg);

                return Observable.throw(errMsg);
            });
    }

    showNotification(message: string) {
        this.toastService.error(message);
    }

    private handleError = (error: any) => {
        return Promise.reject(error.message || error);
    }

    // Show toast as well as login modal
    // use to prevent clicking on login-only content
    showMustBeLoggedInMessage(msg?: string) {
        if (!msg) {
            msg = 'You must login to be able to do that.';
        }
        this.openLoginModal();
        this.showNotification(msg);
    }
}
