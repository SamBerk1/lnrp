import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective, BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { environment } from '../../../environments/environment';

import { AuthService } from '../auth/auth.service';
import { GenreService } from '../../services/genre.service';
import { ReleaseItem } from '../../utils';
import { ReleaseService } from '../release/release.service';
import { WavePlayerService } from '../waveplayer/waveplayer.service';

import * as $ from 'jquery';

@Component({
    selector: 'detail-form-modal',
    templateUrl: './detail-form.component.html'
})
export class DetailFormComponent implements OnInit {
    @ViewChild('childModal') public dropboxModal: ModalDirective;

    id: number;
    img_src: string;
    release_detail: any = {};

    constructor(
        private releaseService: ReleaseService,
        private waveplayerService: WavePlayerService,
        private authService: AuthService,
        private toastService: ToastrService,
        private bsModalRef: BsModalRef,
        private bsModalService: BsModalService,
    ) { }

    ngOnInit() { }

    get isLoggedIn(): Boolean {
        return this.authService.isLoggedIn;
    }

    rateRelease = async (id, rating) => {
        this.releaseService.rateRelease(id, rating)
            .then((r) => {
                // Update the object with the new rating information
                this.release_detail['ratings'] = r.ratings;
                this.release_detail['score'] = r.score;
                this.release_detail['rating'] = r.rating;
                this.release_detail['rated'] = true;

                if (r.crateq) {
                    this.releaseService.CQDownloads = r.crateq;
                }
            });

        let cnt = 0;
        for (const v of this.release_detail.versions) {
            const res = await this.releaseService.checkQueueVersion(v.id);
            v.crateq = res.id;
            if (res.id > 0) {
                cnt++;
            }
        }
        this.release_detail.crateq = this.release_detail.versions.length === cnt;
    }

    requestPlayVersion = (evt: Event, id: Number) => {
        evt.preventDefault();
        this.release_detail.image = this.img_src;
        this.waveplayerService.requestVersion(id);
        this.waveplayerService.requestDetail(this.release_detail);
    }

    requestDownloadRelease = (release: number) => {
        this.releaseService.requestReleaseCode(release)
            .then((r) => {
                $('#download_container').attr('src', environment.crateweb + '/download/' + r.code);
            });
    }

    requestDownloadVersion = (release: number, version: number) => {
        this.releaseService.requestReleaseCode(release, version)
            .then((r) => {
                $('#download_container').attr('src', environment.crateweb + '/download/' + r.code);
            });
    }

    requestDropboxRelease = (release: number) => {
        if (this.authService.user['dropbox'] === null) {
            this.dropboxModal.show();

        } else {
            this.releaseService.requestDropbox(release)
                .subscribe(
                    (success) => {
                        if (success.success) {
                            this.toastService.success(success.msg, '', {
                                disableTimeOut: false,
                                timeOut: 3000
                            });
                        } else {
                            this.toastService.error(success.msg);
                        }
                    },
                    (error) => {
                        const err = error.json();
                        this.toastService.error(err.msg);
                    }
                );
        }
    }

    requestDropboxVersion = (release: number, version: number) => {
        if (this.authService.user['dropbox'] === null) {
            this.dropboxModal.show();

        } else {
            this.releaseService.requestDropbox(release, version)
                .subscribe(
                    (success) => {
                        if (success.success) {
                            this.toastService.success(success.msg, '', {
                                disableTimeOut: false,
                                timeOut: 3000
                            });
                        } else {
                            this.toastService.error(success.msg);
                        }
                    },
                    (error) => {
                        const err = error.json();
                        this.toastService.error(err.msg);
                    }
                );
        }
    }

    requestCrateQRelease = async (release: number, versions: any, isCrateQ: boolean) => {
        if (isCrateQ) {
            let cnt = 0;
            for (let version of versions) {
                if (version.crateq === 0) {
                    cnt++;                    
                } else {
                    const res = await this.releaseService.removeQueueVersion(version.id);
                    if (res.id != undefined) {
                        version.crateq = res.id;
                        if (res.id === 0) {
                            cnt++;
                        }
                    }
                }
            }
            if (cnt > 0) {
                this.release_detail.crateq = false;
            }
            if (cnt === versions.length) {
                this.toastService.success('The song was successfully removed from crateQ queue', '', {
                    disableTimeOut: false,
                    timeOut: 3000
                });
            }
        } else {
            const response = await this.releaseService.requestCrateQ(release);
            if (response.success) {
                let cnt = 0;
                for (let version of versions) {
                    if (version.crateq > 0) {
                        cnt++;
                    } else {
                        const res = await this.releaseService.addQueueVersion(version.id);
                        if (res.id != undefined) {
                            version.crateq = res.id;
                            if (res.id > 0) {
                                cnt++;
                            }
                        }
                    }
                }
                if (cnt === versions.length) {
                    this.release_detail.crateq = true;
                }
                this.toastService.success(response.msg, '', {
                    disableTimeOut: false,
                    timeOut: 3000
                });
            }
        }
    }

    requestCrateQVersion = async (release: number, version: number, vCrateQ: number) => {
        if (vCrateQ > 0) {
            const res = await this.releaseService.removeQueueVersion(version);
            if (res.id != undefined) {
                let cnt = 0;
                for (let v of this.release_detail.versions) {
                    if (v.id === version) {
                        v.crateq = res.id;
                    }
                    if (v.crateq === 0) {
                        cnt++;
                    }
                }
                if (cnt > 0) {
                    this.release_detail.crateq = false;
                }
            }
        } else {
            const response = await this.releaseService.requestCrateQ(release, version);
            if (response.success) {
                const res = await this.releaseService.addQueueVersion(version);
                if (res.id != undefined) {
                    let cnt = 0;
                    for (let v of this.release_detail.versions) {
                        if (v.id === version) {
                            v.crateq = res.id;
                        }
                        if (v.crateq > 0) {
                            cnt++;
                        }
                    }
                    if (cnt === this.release_detail.versions.length) {
                        this.release_detail.crateq = true;
                    }
                    this.toastService.success(response.msg, '', {
                        disableTimeOut: false,
                        timeOut: 3000
                    });    
                }
            }    
        }
    }

    openRemix = (item: ReleaseItem) => {
        const remix: ReleaseItem = {
            ...item.remixes,
            image: environment.apiURL + '/images/src/' + item.remix,
        }
        this.bsModalRef.hide();
        this.bsModalRef = this.bsModalService.show(DetailFormComponent, { class: 'detail-form' });
        this.bsModalRef.content.id = remix.id;
        this.bsModalRef.content.img_src = remix.image;
        this.bsModalRef.content.release_detail = remix;
    }

    closeDropboxModal = () => {
        this.dropboxModal.hide();
    }

    closeModal = () => {
        this.bsModalRef.hide();
    }
}
