import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { environment } from '../../../environments/environment';

import { AuthService } from '../auth/auth.service';
import { GenreService } from '../../services/genre.service';
import { ReleaseService } from '../release/release.service';
import { WavePlayerService } from '../waveplayer/waveplayer.service';
import { DetailFormComponent } from '../detail-form/detail-form.component';

@Component({
    selector: 'app-trend',
    templateUrl: './trend.component.html'
})

export class TrendComponent implements OnInit, AfterViewInit {
    public _genre: number = 0;
    public _subgenre: number = 0;

    public first_row: Object[];
    public second_row: Object[];
    public third_row: Object[];

    public releases: Object[];
    public allReleases: Object[];

    releaseDetails = [];
    top_title: string = 'all Genres';

    detailFromModalRef: BsModalRef;

    constructor(
        private route: ActivatedRoute,
        private releaseService: ReleaseService,
        private waveplayerService: WavePlayerService,
        private authService: AuthService,
        private modalService: BsModalService
    ) {}

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this._genre = +params['genre'] || -1;
            this._subgenre = +params['subgenre'] || -1;

            this.requestTopDownloads(this._genre, this._subgenre);
        });
        this.authService.loginAnnounce$.subscribe(() => {
            this.prearrangeData();
        });
        this.authService.logoutAnnounce$.subscribe(() => {
            this.prearrangeData();
        });
    }

    ngAfterViewInit() {}

    get isLoggedIn(): Boolean {
        return this.authService.isLoggedIn;
    }

    triggerLoginModal = () => {
        this.authService.openLoginModal();
    }

    requestPlayRelease = (evt:any, release) => {
        evt.preventDefault();

        this.waveplayerService.requestRelease(release.id);
        this.waveplayerService.requestDetail(release);
    }

    openDetail (item) {
        this.detailFromModalRef = this.modalService.show(DetailFormComponent, { class: 'detail-form' });
        this.detailFromModalRef.content.id = item.id;
        this.detailFromModalRef.content.img_src = item.image;
        this.detailFromModalRef.content.release_detail = item;    
    }

    private requestTopDownloads = (genre: number, subgenre: number) => {
        this.releaseService.getRecentTopDownloads().then((response) => {
            this.allReleases = response.releases;

            this.allReleases.forEach((r, key) => {
                r['image'] = environment.apiURL + '/images/src/' + r['id'];
                r['number'] = key + 1;
            });

            this.prearrangeData();
        });
    }

    private prearrangeData () {
        this.releases = [];
        this.allReleases.forEach(item => {
            this.releases.push(item);
        });
        this.first_row = this.releases.splice(
            0,
            this.isLoggedIn ? 2 : 1
        );

        this.second_row = this.releases.splice(0, 2);
        this.third_row = this.releases.splice(0, 2);
    }
}
