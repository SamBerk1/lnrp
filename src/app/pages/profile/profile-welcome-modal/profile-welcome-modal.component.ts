import { Component, OnInit } from '@angular/core';
import { SignupService } from '../../signup/signup.service';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-profile-welcome-modal',
  templateUrl: './profile-welcome-modal.component.html',
  styleUrls: ['./profile-welcome-modal.component.scss']
})
export class ProfileWelcomeModalComponent {

  constructor(
    private _signupService: SignupService,
    private _bsModalRef: BsModalRef,
  ) { }

  public onAgree() {
    this._signupService.userProfileWelcomed();
    this._bsModalRef.hide();
  }

}
