import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserAccount } from '../../utils';
import { JWTObject, HTTPInterceptor } from '../../services';
import { AuthService } from '../auth/auth.service';
import { SignupService } from '../signup/signup.service';
import { BsModalService } from 'ngx-bootstrap';
import { ProfileWelcomeModalComponent } from './profile-welcome-modal/profile-welcome-modal.component';
import { AccountService } from '../account/account.service';

@Component({
  selector: 'profile-page',
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit, AfterViewInit {

  user: UserAccount;

  account: any = {};

  submitted: boolean = false;

  constructor(
    private authService: AuthService,
    private signupService: SignupService,
    private toastService: ToastrService,
    private _modalService: BsModalService,
    private _accountService: AccountService,
    private newHttp: HTTPInterceptor,
    private router: Router,
  ) {
    this.account = <JWTObject>(this.newHttp.jwt).getAccount();
    if (this.account === undefined || !this.account) {
      this.router.navigateByUrl('/');
      return;
    }
  }

  ngOnInit() {
    this.user = this.initUser();
    this.authService.logoutAnnounce$
      .subscribe(
        () => {
          this.router.navigateByUrl('/');
        }
      );
  }

  ngAfterViewInit() {
    if (this.signupService.showUserWelcomeModal()) {
      this._modalService.show(ProfileWelcomeModalComponent, {ignoreBackdropClick: true});
    }
  }

  updateUser = () => {
    this.submitted = true;
    this.signupService.updateUser(this.user)
      .subscribe(
        (response) => {
          this.user = response.json();
          this.submitted = false;
          this.toastService.success('Your profile was successfully saved!');
          this.router.navigateByUrl('/features');
        },
        (err) => {
          this.toastService.error('Saving your profile was failed.')
          this.router.navigateByUrl('/new');
          console.error(err);
        }
      );
  }

  public userOptOutOfProfile() {
    this._accountService.userOptOutOfCompletingProfile();
    this.router.navigateByUrl('/new');
  }

  private initUser(): UserAccount {
    return {
      id: this.account.id,
      date: this.account.date,
      vipick: this.account.vipick,
      enabled: this.account.enabled,
      approved: this.account.approved,
      newsletter: this.account.newsletter,
      type: this.account.type,
      image: this.account.image,
      username: this.account.username,
      password: '',
      old_password: '',
      name: this.account.name,
      djname: this.account.djname,
      affiliation: this.account.affiliation,
      address: this.account.address,
      city: this.account.city,
      state: this.account.state,
      zip: this.account.zip,
      phone: this.account.phone,
      email: this.account.email,
      facebook: this.account.facebook,
      twitter: this.account.twitter,
      youtube: this.account.youtube,
      soundcloud: this.account.soundcloud,
      instagram: this.account.instagram,
      website: this.account.website,
      equipment_mac: this.account.equipment_mac,
      equipment_pc: this.account.equipment_pc,
      equipment_turntables: this.account.equipment_turntables,
      software: this.account.software,
      controller: this.account.controller,
      radiostation: this.account.radiostation == 'yes',
      bds: this.account.bds,
      media: this.account.media,
      callletters: this.account.callletters,
      showname: this.account.showname,
      showtime: this.account.showtime,
      programdirector: this.account.programdirector,
      pdmdphone: this.account.pdmdphone,
      pdmdemail: this.account.pdmdemail,
      club: this.account.club == 'yes',
      clubname: this.account.clubname,
      clubnights: this.account.clubnights,
      clubtimes: this.account.clubtimes,
      house: this.account.house == 'yes',
      festival: this.account.festival == 'yes',
      festivalworked: this.account.festivalworked,
      topgenres: this.account.topgenres,
      topartists: this.account.topartists,
      topproducers: this.account.topproducers,
      influences: this.account.influences,
      desertisland: this.account.desertisland,
      referral_type: this.account.referral_type,
      referral_value: this.account.referral_value,
      subscription: this.account.subscription,
      paypal_account: this.account.paypal_account,
      paypal_approve: this.account.paypal_approve,
      paypal_date: this.account.paypal_date,
      stripe_id: this.account.stripe_id,
      stripe_subscription: this.account.stripe_subscription,
      payment_type: this.account.payment_type,
      card_type: this.account.card_type,
      card_last_four: this.account.card_last_four,
      card_exp: this.account.card_exp,
      ip_address: this.account.ip_address,
      created_at: this.account.created_at,
      updated_at: this.account.updated_at,
    }
  }
}
