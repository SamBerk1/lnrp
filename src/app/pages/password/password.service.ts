import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { ToastrService } from 'ngx-toastr';

import { environment } from '../../../environments/environment';

import { JWTObject, HTTPInterceptor } from '../../services';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ResetPasswordService {
    constructor(
        private newHttp: HTTPInterceptor,
        private toastService: ToastrService
    ) {}

    resetPassword = (form: any) => {
        return this.newHttp
            .post(`${environment.apiURL}/user/reset/password`, form)
            .do(
                (res) => {},
                (error) => {
                    let errMsg: string;

                    if (error instanceof Response) {
                        let body = error.json() || '';
                        errMsg = `${body.error || ''}`;

                    } else {
                        errMsg = error.message ? error.message : error.toString();
                    }

                    this.toastService.error(errMsg);
                    
                    return Observable.throw(errMsg);
                },
                () => {}
            );
    }
}
