import { Component, ViewChild, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { environment } from '../../../environments/environment';

import { JWTObject, HTTPInterceptor } from '../../services';
import { ResetPasswordService } from './password.service';

@Component({
    selector: 'password-reset',
    templateUrl: './password.component.html'
})
export class ResetPasswordComponent implements OnInit {
    jwt: JWTObject;
    account: Object;

    reset_code: string = '';

    constructor(
        private resetService: ResetPasswordService,
        private toastService: ToastrService,
        private advancedRoute: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit() {
        this.advancedRoute.params.subscribe((params) => {
            this.reset_code = params['code'] || '';
        });
}

    sendResetForm = (form: any) => {
        this.resetService.resetPassword(form)
            .subscribe(
                (success) => {
                    this.toastService.success('Your password has been successfully reset!');

                    this.router.navigateByUrl('/');
                }
            )
    }
}
