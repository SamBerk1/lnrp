import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { SelectModule } from 'ng2-select';
import { PaginationInstance } from 'ngx-pagination';

import { environment } from '../../../environments/environment';
import { ReleaseConstants } from '../../utils';
import { RatingModule } from '../../modules/ratings.module';

import { AuthService } from '../auth/auth.service';
import { GenreService } from '../../services/genre.service';

@Component({
    selector: 'pricing-page',
    templateUrl: './pricing.component.html'
})

export class PricingComponent implements OnInit, AfterViewInit {

    constructor() {}

    ngOnInit() {}

    ngAfterViewInit() {}
}
