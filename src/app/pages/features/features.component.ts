import { Component, OnInit } from '@angular/core';
import { FrontpageService } from '../frontpage/frontpage.service';
import { UserAccount } from '../../utils';
import { JWTObject, HTTPInterceptor } from '../../services';
import { AccountService } from '../account/account.service';
import { environment } from '../../../environments/environment';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss']
})
export class FeaturesComponent {

  public features: any[];
  public account: UserAccount;
  jwt: JWTObject;

  constructor(
    private _frontpageService: FrontpageService,
    private _accountService: AccountService,
    private _authService: AuthService,
    private _newHttp: HTTPInterceptor,
  ) {
    this.features = this._frontpageService.getFeatureSet();
    this.account = this._accountService.getCurrentAccount();

    if (this.dropboxConnected) {
      this.getDropboxAccount();
    }
  }

  get dropboxConnected() {
    return this.account['dropbox'] !== null;
  }
  
  openCrateQDowload() {
    window.open(environment.crateQDownloader, '_blank');
  }
  openDropboxLink = () => {
    window.location.href = environment.dropboxweb + '/dropbox/connect/' + this.account.id;
  }
  unlinkDropboxAccount() {
    this._accountService.unlinkDropboxAccount(this.account).then((result) => {
      this.refreshAccount();
    })
  }

  refreshAccount = () => {
    this._authService.refreshUserToken().subscribe((success) => {
      this.jwt = this._newHttp.jwt;
      this.account = this.jwt['payload'] as UserAccount;
      if (this.account['dropbox']) {
        this.getDropboxAccount();
      }
    });
  }

  getDropboxAccount() {
    this._accountService.getDropboxInfo(this.account['dropbox']).subscribe((res) => {
      this.account['dropbox_info'] = res;
    }, (err) => {
      err = err.json();
      console.log('---', err);
      if (err.error['.tag'] == 'invalid_access_token') {
        this._accountService.unlinkDropbox(this.account).subscribe((result) => {
          this.refreshAccount();
        });
      }
    });
  }

}
