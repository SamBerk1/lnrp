import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { UserAccount } from '../../utils';
import { JWTObject, HTTPInterceptor, GeneralService } from '../../services';
import { SignupService } from './signup.service';
import { AuthService } from '../auth/auth.service';
import { ReleaseService } from '../release/release.service';

import * as $ from 'jquery';
import { BsModalService } from 'ngx-bootstrap';
import { SignupDisclaimerModalComponent } from './signup-disclaimer-modal/signup-disclaimer-modal.component';

declare var braintree: any;
declare const fbq: any;
declare var ga: Function;

@Component({
    selector: 'signup',
    templateUrl: './signup.component.html'
})
export class SignupComponent implements OnInit, AfterViewInit {
    current_page: number = 1;
    page_submitted: Boolean = false;
    social_link_required: Boolean = false;
    referral_type_required: Boolean = false;

    userAccount: UserAccount;

    accountForm: FormGroup;
    djForm: FormGroup;
    optionalForm: FormGroup;
    paymentForm: FormGroup;
    subscriptionForm: FormGroup;

    clientCode: string = '';
    promoCode: string = '';

    customBilling: any = {}
    isNewBilling: boolean = false;

    errors: Object = ErrorMessages;

    braintreeFieldsInstance: any;
    braintreeNonce: string;

    creditCard: Object = {
        cardNumber: '',
        cardExpMonth: '05',
        cardExpYear: '2019',
        cardCVV: '',
        cardZip: ''
    };

    isDesktop: boolean = false;

    // Also, don't forget to validate and submit the form as you go, create on the first step and updates thereafter.

    constructor(
        private fb: FormBuilder,
        private newHttp: HTTPInterceptor,
        private signupService: SignupService,
        private toastService: ToastrService,
        private authService: AuthService,
        private router: Router,
        private releaseService: ReleaseService,
        private generalService: GeneralService,
        private _modalService: BsModalService,
    ) {
        // this.current_page = 3;
        this.setupAccountForm();
        this.setupDJForm();
        this.setupOptionalForm();
        this.setupPaymentForm();

        // Don't need this anymore since we're not using Braintree.
        // If we ever do, be sure to un-comment the js files in the index
        // this.getClientCode();

        this.userAccount = signupService.generateNewUser();
        this.generalService.initial();
        this.isDesktop = this.generalService.isDesktop;
    }

    ngOnInit() {
        this.authService.loginAnnounce$
            .subscribe(
                () => {
                    this.router.navigateByUrl('/');
                }
            );

        this.generalService.toggleDesktop$.subscribe(isDesktop => {
            this.isDesktop = isDesktop;
        })
    }

    ngAfterViewInit() {
        this._modalService.show(SignupDisclaimerModalComponent, {ignoreBackdropClick: true});
    }

    nextPage = () => {
        switch (this.current_page) {
            case 1:

                if (this.accountForm.invalid) {
                    this.page_submitted = true;
                    this.toastService.error('Your form is not complete. Please check the information entered and try again');
                    return;
                }

                if (this.userAccount.facebook == '' && this.userAccount.twitter == '' &&
                    this.userAccount.youtube == '' && this.userAccount.soundcloud == '' &&
                    this.userAccount.instagram == '' && this.userAccount.website == '') {
                    this.social_link_required = true;
                    this.toastService.error('Your form is not complete. Please check the information entered and try again');
                    return;
                }

                if (!this.isDesktop) {
                    if (this.userAccount.referral_type === '') {
                        this.referral_type_required = true;
                        this.toastService.error('We need to verify you a professional working DJ, please fill out all the required fields otherwise your registration will be declined');
                        return;
                    } else if (this.userAccount.referral_type === 'referral' || this.userAccount.referral_type === 'board') {
                        if (this.userAccount.referral_value === '') {
                            this.referral_type_required = true;
                            this.toastService.error('You should input more detail. Pleaes input it and try again');
                            return;
                        }
                    }
                }

                const submitData = {
                    promo: this.promoCode,
                    user: this.userAccount,
                }

                this.signupService.createNewUser(submitData)
                    .subscribe(
                        (response) => {
                            this.userAccount = response.user;
                            this.page_submitted = false;
                            this.current_page++;
                            if (response.isPromo) {
                                this.customBilling = response.billing;
                                this.isNewBilling = true;
                            } else {
                                this.isNewBilling = false;
                                this.customBilling = {};
                            }
                            this.scrollTopIfMobile();
                        },
                        (err) => {
                            console.error(err.json());
                            let errMessage = err.json();
                            this.toastService.error(errMessage.error);
                        }
                    );
                break;

            case 2:
                if (this.djForm.invalid) {
                    this.page_submitted = true;
                    this.toastService.error('Your form is not complete. Please check the information entered and try again');
                    return;
                }

                this.signupService.updateUser(this.userAccount)
                    .subscribe(
                        (response) => {
                            this.userAccount = response.json();

                            this.page_submitted = false;
                            this.current_page++;
                            this.scrollTopIfMobile();
                        },
                        (err) => {
                            console.error(err);
                        }
                    );
                break;

            case 3:
                if (this.optionalForm.invalid) {
                    this.page_submitted = true;
                    this.toastService.error('Your form is not complete. Please check the information entered and try again');
                    return;
                }

                this.signupService.updateUser(this.userAccount)
                    .subscribe(
                        (response) => {
                            this.userAccount = response.json();

                            // Reset subscription package for the next page
                            if (this.isNewBilling) {
                                this.userAccount.subscription = this.customBilling.stripe_id;
                            } else {
                                this.userAccount.subscription = 'lnrp_monthly';
                            }

                            this.page_submitted = false;
                            this.current_page++;
                            this.scrollTopIfMobile();
                        },
                        (err) => {
                            console.error(err);
                        }
                    );
                break;

            case 4:
                // If you're on page 4, the completeSignup button is
                // exposed and not the nextPage button
                break;
        }
    }

    updateUser = () => {
        this.signupService.updateUser(this.userAccount)
            .subscribe(
                (response) => {
                    this.userAccount = response.json();
                    this.page_submitted = false;
                    this.toastService.success('Updating your account information was succeeded!');
                    this.router.navigateByUrl('/');
                },
                (err) => {
                    console.error(err);
                }
            );
    }

    previousPage = (evt: any) => {
        evt.preventDefault();
        this.current_page--;
    }

    completeSignup = () => {
        if (this.paymentForm.invalid) {
            this.page_submitted = true;
            this.toastService.error('Your form is not complete. Please check the information entered and try again');
            return;
        }

        fbq('track', 'createAccount');
        ga('send', 'event', {
            eventCategory: 'createAccount',
            eventAction: 'submit'
        });

        this.creditCard['subscription'] = this.userAccount.subscription;

        this.signupService.completeUser(this.userAccount, this.creditCard)
            .subscribe(
                (success) => {
                    // this.authService.isLoggedIn = true;
                    if (this.current_page === 2) {
                        this.current_page++;
                        this.toastService.success('Your account has been successfully created! We will review your registration within 24-48 hours. Thank you for choosing Late Night Record Pool!');
                        this.scrollTop();
                        return;
                    }
                    this.router.navigateByUrl('/new');
                    this.toastService.success('Your account has been successfully created! We will review your registration within 24-48 hours. Thank you for choosing Late Night Record Pool!');
                },
                (err) => {
                    console.error(err);
                }
            );
    }

    openRenewModal = (evt: any) => {
        evt.preventDefault();

        this.authService.openRenewModal();
    }

    focusedInputField = () => {
        this.social_link_required = false;
    }

    focusedReferral = () => {
        this.referral_type_required = false;
    }

    setLabel = (label: string) => {
        this.releaseService.setLabel(label);
        if (this.router.routerState.snapshot.url === '/releases') {
            this.releaseService.requestReleasesBroadcast();
        } else {
            this.router.navigate(['releases']);
        }
    }

    onGoSignup = (el, label: string) => {
        fbq('track', label);
        ga('send', 'event', {
            eventCategory: label,
            eventLabel: 'signup',
            eventAction: 'click'
        });

        el.scrollIntoView({ block: 'start', behavior: 'smooth' });
    }

    private setupAccountForm = () => {
        this.accountForm = this.fb.group({
            name: new FormControl(null, Validators.required),
            djname: new FormControl(null, Validators.required),
            address: '',
            city: '',
            state: '',
            zip: '',
            username: new FormControl(null, Validators.required),
            password: new FormControl(null, [Validators.required, Validators.minLength(3)]),
            phone: new FormControl(null, Validators.required),
            email: new FormControl(null, [Validators.required, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)]),
            facebook: '',
            twitter: '',
            youtube: '',
            soundcloud: '',
            instagram: '',
            website: '',
            find: '',
            referral: '',
            board: '',
        });
    }

    private setupDJForm = () => {
        this.djForm = this.fb.group({
            equipmentMac: '',
            equipmentPC: '',
            equipmentTurntables: '',
            software: '',
            controller: '',

            typeRadio: '',
            radioBDS: '',
            radioMediabase: '',
            radioCall: '',
            radioName: '',
            radioTime: '',
            radioPD: '',
            radioPDPhone: '',
            radioPDEmail: '',

            typeClub: '',
            clubName: '',
            clubNights: '',
            clubTimes: '',

            typeHouse: '',

            typeFestival: '',
            festivalWorked: ''
        });
    }

    private setupOptionalForm = () => {
        this.optionalForm = this.fb.group({
            top_genres: '',
            top_artists: '',
            top_producers: '',
            influences: '',
            top_desert_island: '',
            find: '',
            referral: '',
            board: ''
        });
    }

    private setupPaymentForm = () => {
        this.paymentForm = this.fb.group({
            paymentType: 'Stripe',
            subscription: 'lnrp_monthly',
            cardNumber: new FormControl(null, Validators.required),
            cardExpMonth: new FormControl('05', Validators.required),
            cardExpYear: new FormControl('2021', Validators.required),
            cardCVV: new FormControl(null, Validators.required),
            cardZip: new FormControl(null, Validators.required)
        });
    }

    private getClientCode = () => {
        let context = this;

        /* ------------
        // This code should be added back to the completeSignup button if you want to use Braintree
        //

            // First, we need to send off the payment info to Braintree in order
            // to get the payment nonce
            this.braintreeFieldsInstance
                .tokenize((tokenizeErr, payload) => {
                    if (tokenizeErr) {
                        console.error(tokenizeErr);
                        return;
                    }

                    // The, we store all the needed information...
                    this.braintreeNonce = payload.nonce;

                    this.creditCard['cardType'] = payload['details']['cardType'];
                    this.creditCard['lastTwo'] = payload['details']['lastTwo'];

                    // Send an update call to add the remaining user information

                    // And activate the user!
                });
        // --------------- */

        this.signupService.getClientCode()
            .subscribe(
                (success) => {
                    let codeObj = success.json();

                    this.clientCode = codeObj['code'];

                    braintree.client.create({
                        authorization: this.clientCode
                    }, (clientErr, clientInstance) => {
                        if (clientErr) {
                            console.error(clientErr);
                        }

                        braintree.hostedFields.create({
                            client: clientInstance,
                            styles: {
                                'input': {
                                    'color': 'white',
                                    'font-size': '14px'
                                },
                                'input.invalid': {
                                    'color': 'red'
                                },
                                'input.valid': {
                                    'color': 'green'
                                }
                            },
                            fields: {
                                number: {
                                    selector: '#payment_card',
                                    placeholder: '4111 1111 1111 1111'
                                },
                                cvv: {
                                    selector: '#payment_cvv',
                                    placeholder: '123'
                                },
                                expirationDate: {
                                    selector: '#payment_expiration',
                                    placeholder: '10/2019'
                                },
                                postalCode: {
                                    selector: '#payment_zip',
                                    placeholder: '11111'
                                }
                            }
                        }, function (hostedErr, hostedInstance) {
                            if (hostedErr) {
                                console.error(hostedErr);
                                return;
                            }

                            context.braintreeFieldsInstance = hostedInstance;
                        });
                    });
                }
            );
    }

    private scrollTop() {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    }

    private scrollTopIfMobile() {
        if (!this.isDesktop) {
            this.scrollTop();
        }
    }

    get validatePageAccount() {
        let pageValid: Boolean = true;

        if (!this.accountForm.valid) {
            Object.keys(this.accountForm.controls).forEach((c) => {
                let control: AbstractControl = this.accountForm.controls[c];
                let controlName: string = c;

                if (!control.valid) {
                    pageValid = false;

                    Object.keys(control.errors).forEach((err) => {
                        this.errors[`${controlName}.${err}`] = ErrorMessages[`${controlName}.${err}`];
                    });
                }
            });
        }

        return pageValid;
    }

    states: Object[] = [
        {
            'name': 'Alabama',
            'abbreviation': 'AL'
        }, {
            'name': 'Alaska',
            'abbreviation': 'AK'
        }, {
            'name': 'Arizona',
            'abbreviation': 'AZ'
        }, {
            'name': 'Arkansas',
            'abbreviation': 'AR'
        }, {
            'name': 'California',
            'abbreviation': 'CA'
        }, {
            'name': 'Colorado',
            'abbreviation': 'CO'
        }, {
            'name': 'Connecticut',
            'abbreviation': 'CT'
        }, {
            'name': 'Delaware',
            'abbreviation': 'DE'
        }, {
            'name': 'District Of Columbia',
            'abbreviation': 'DC'
        }, {
            'name': 'Florida',
            'abbreviation': 'FL'
        }, {
            'name': 'Georgia',
            'abbreviation': 'GA'
        }, {
            'name': 'Guam',
            'abbreviation': 'GU'
        }, {
            'name': 'Hawaii',
            'abbreviation': 'HI'
        }, {
            'name': 'Idaho',
            'abbreviation': 'ID'
        }, {
            'name': 'Illinois',
            'abbreviation': 'IL'
        }, {
            'name': 'Indiana',
            'abbreviation': 'IN'
        }, {
            'name': 'Iowa',
            'abbreviation': 'IA'
        }, {
            'name': 'Kansas',
            'abbreviation': 'KS'
        }, {
            'name': 'Kentucky',
            'abbreviation': 'KY'
        }, {
            'name': 'Louisiana',
            'abbreviation': 'LA'
        }, {
            'name': 'Maine',
            'abbreviation': 'ME'
        }, {
            'name': 'Maryland',
            'abbreviation': 'MD'
        }, {
            'name': 'Massachusetts',
            'abbreviation': 'MA'
        }, {
            'name': 'Michigan',
            'abbreviation': 'MI'
        }, {
            'name': 'Minnesota',
            'abbreviation': 'MN'
        }, {
            'name': 'Mississippi',
            'abbreviation': 'MS'
        }, {
            'name': 'Missouri',
            'abbreviation': 'MO'
        }, {
            'name': 'Montana',
            'abbreviation': 'MT'
        }, {
            'name': 'Nebraska',
            'abbreviation': 'NE'
        }, {
            'name': 'Nevada',
            'abbreviation': 'NV'
        }, {
            'name': 'New Hampshire',
            'abbreviation': 'NH'
        }, {
            'name': 'New Jersey',
            'abbreviation': 'NJ'
        }, {
            'name': 'New Mexico',
            'abbreviation': 'NM'
        }, {
            'name': 'New York',
            'abbreviation': 'NY'
        }, {
            'name': 'North Carolina',
            'abbreviation': 'NC'
        }, {
            'name': 'North Dakota',
            'abbreviation': 'ND'
        }, {
            'name': 'Ohio',
            'abbreviation': 'OH'
        }, {
            'name': 'Oklahoma',
            'abbreviation': 'OK'
        }, {
            'name': 'Oregon',
            'abbreviation': 'OR'
        }, {
            'name': 'Pennsylvania',
            'abbreviation': 'PA'
        }, {
            'name': 'Rhode Island',
            'abbreviation': 'RI'
        }, {
            'name': 'South Carolina',
            'abbreviation': 'SC'
        }, {
            'name': 'South Dakota',
            'abbreviation': 'SD'
        }, {
            'name': 'Tennessee',
            'abbreviation': 'TN'
        }, {
            'name': 'Texas',
            'abbreviation': 'TX'
        }, {
            'name': 'Utah',
            'abbreviation': 'UT'
        }, {
            'name': 'Vermont',
            'abbreviation': 'VT'
        }, {
            'name': 'Virginia',
            'abbreviation': 'VA'
        }, {
            'name': 'Washington',
            'abbreviation': 'WA'
        }, {
            'name': 'West Virginia',
            'abbreviation': 'WV'
        }, {
            'name': 'Wisconsin',
            'abbreviation': 'WI'
        }, {
            'name': 'Wyoming',
            'abbreviation': 'WY'
        }
    ];
}

export const ErrorMessages = {
    'name.required': 'A Name is required',
    'djname.required': 'A DJ Name is required',
    'username.required': 'A Username is required',
    'password.required': 'A Password is required',
    'password.minlength': 'A Password must be more than 3 characters',
    'email.required': 'An Email Address is required',
    'email.pattern': 'A valid Email Address is required',
    'phone.required': 'A Phone Number is required',
    'card.number.required': 'A valid credit card number is required',
    'card.cvv.required': 'A valid CVV code for your card is required',
    'card.zip.required': 'A valid Zip Code is required',
    'social.required': 'Social or website link is required'
}
