import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions } from '@angular/http';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

import { environment } from '../../../environments/environment';

import { HTTPInterceptor } from '../../services/interceptor.service';
import { AuthService } from '../auth/auth.service';
import { UserAccount } from '../../utils';

export interface Billing {
    enabled: boolean
    paypal_code: string
    paypal_id: string
    paypal_title: string
    stripe_id: string
    stripe_title: string
}

@Injectable()
export class SignupService {
    private billingSource = new Subject<Billing>();
    billingSource$ = this.billingSource.asObservable();

    constructor(
        private newHttp: HTTPInterceptor,
        private authService: AuthService,
        private toastService: ToastrService
    ) {}

    getClientCode = () => {
        return this.newHttp
                   .get(`${environment.apiURL}/braintree/client-code`)
                   .catch(this.handleError);
    }

    createUser = (user: Object) => {
        return this.newHttp
                   .post(`${environment.apiURL}/user`, user);
    }

    createNewUser = (user: Object) => {
        return this.newHttp
                   .post(`${environment.apiURL}/user/new`, user).pipe(map(res => res.json()));
    }

    validationPromo = (data: Object) => {
        return this.newHttp
                    .post(`${environment.apiURL}/user/promo`, data).pipe(map(res => res.json()))
                    .catch(this.handleError);
    }

    updateUser = (user: Object) => {
        // if (user['id'])
        return this.newHttp
                   .put(`${environment.apiURL}/user/${user['id']}`, user)
                   .catch(this.handleError);
    }

    completeUser = (user: UserAccount, creditCard: Object) => {
        return this.newHttp
                   .put(`${environment.apiURL}/user/complete/${user.id}`, creditCard)
                   .catch(this.handleError);
    }

    generateNewUser = (): UserAccount => {
        var m = new Date();
        var dateString =
            m.getUTCFullYear() + '-' +
            ('0' + (m.getUTCMonth()+1)).slice(-2) + '-' +
            ('0' + m.getUTCDate()).slice(-2) + ' ' +
            ('0' + m.getUTCHours()).slice(-2) + ':' +
            ('0' + m.getUTCMinutes()).slice(-2) + ':' +
            ('0' + m.getUTCSeconds()).slice(-2);

        return {
            date: dateString,
            vipick: false,
            enabled: false,
            approved: false,
            type: 'In Progress',
            image: '',
            username: '',
            password: '',
            old_password: '',
            name: '',
            djname: '',
            affiliation: '',
            address: '',
            city: '',
            state: 'MO',
            zip: '',
            phone: '',
            email: '',
            facebook: '',
            twitter: '',
            youtube: '',
            soundcloud: '',
            instagram: '',
            equipment_mac: false,
            equipment_pc: false,
            equipment_turntables: false,
            software: '',
            controller: '',
            radiostation: false,
            bds: '',
            media: '',
            callletters: '',
            showname: '',
            showtime: '',
            programdirector: '',
            pdmdphone: '',
            pdmdemail: '',
            club: false,
            clubname: '',
            clubnights: '',
            clubtimes: '',
            house: false,
            festival: false,
            festivalworked: '',
            topgenres: '',
            topartists: '',
            topproducers: '',
            influences: '',
            desertisland: '',
            referral_type: '',
            referral_value: '',
            subscription: 'lnrp_monthly',
            paypal_account: '',
            paypal_approve: false,
            paypal_date: '',
            stripe_id: '',
            stripe_subscription: '',
            payment_type: 'Stripe',
            card_type: '',
            card_last_four: '',
            card_exp: '',
            website: '',
            ip_address: localStorage.getItem('lnrp-ip')
        }
    }

    private handleError = (error: any) => {
        if (!error.ok) {
            switch (error.status) {
                case 400:
                    this.toastService.error('An error occurred trying to perform this action. Please try again or contact support.');
                    break;

                case 401:
                    this.authService.registerLogout();
                    this.toastService.warning('Your login has expired. Please log back in and try again.');
                    break;

                case 404:
                    this.toastService.error(error._body);
                    break;

                case 500:
                    this.toastService.error('Sorry, you have a server side issue. Please contact support.');
                    break;

                default:
                    // Do nothing, yet.
                    break;
            }
        }

        return Observable.throw(error._body || error);
        // return Promise.reject(error.message || error);
    }

    setBillingSource(billing: Billing) {
        this.billingSource.next(billing);
    }

    // store in local cache that user has agreed
    public userAgreedToDisclaimer(): void {
        localStorage.setItem('lnrp-user-agreed-to-disclaimer', 'true');
    }

    // NOT NEEDED, but keeping just in case
    // user does not need to see the disclaimer again if they have accepted it once
    public showUserDisclaimer(): boolean {
        return localStorage.getItem('lnrp-user-agreed-to-disclaimer') !== 'true';
    }
    public showUserWelcomeModal(): boolean {
        return localStorage.getItem('lnrp-user-profile-welcomed') !== 'true';
    }
    public userProfileWelcomed(): void {
        localStorage.setItem('lnrp-user-profile-welcomed', 'true');
    }
}
