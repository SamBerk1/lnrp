import { Component, OnInit } from '@angular/core';
import { SignupService } from '../signup.service';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-signup-disclaimer-modal',
  templateUrl: './signup-disclaimer-modal.component.html',
  styleUrls: ['./signup-disclaimer-modal.component.scss']
})
export class SignupDisclaimerModalComponent {

  constructor(
    private _signupService: SignupService,
    private _bsModalRef: BsModalRef,
  ) { }

  public onAgree() {
    this._signupService.userAgreedToDisclaimer();
    this._bsModalRef.hide();
  }

}
