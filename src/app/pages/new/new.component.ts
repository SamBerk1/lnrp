import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { SelectModule } from 'ng2-select';
import { PaginationInstance } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { environment } from '../../../environments/environment';

import { ReleaseConstants } from '../../utils';
import { ReleaseItem } from '../../utils';
import { AuthService } from '../auth/auth.service';
import { GenreService } from '../../services/genre.service';
import { ReleaseService } from '../release/release.service';
import { WavePlayerService } from '../waveplayer/waveplayer.service';
import { RatingModule } from '../../modules/ratings.module';
import { DetailFormComponent } from '../detail-form/detail-form.component';

import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
    selector: 'new-release-page',
    templateUrl: './new.component.html',
    encapsulation: ViewEncapsulation.None
})

export class NewReleaseComponent implements OnInit, AfterViewInit {
    @ViewChild('childModal')
    public dropboxModal: ModalDirective;

    detailFromModalRef: BsModalRef;

    private genres: any;
    private decades: Object[];

    private versions: Object[];

    private bpm: Object[];

    private letters: Object[];

    private dates: Object[];

    private throwback: Boolean = false;
    private remix: Boolean = false;

    public release_groups: Array<Object> = [];
    public releases: Array<ReleaseItem> = [];

    public page_title: String = 'New Releases';

    private open_release: Number = 0;

    public sort_options: Object[];

    public filter_params: Object[] = [];

    public none_found: Boolean = false;

    public config: PaginationInstance = {
        id: 'releases_pages',
        itemsPerPage: 25,
        currentPage: 1,
        totalItems: 0
    };

    public current_month: Object = {
        month: '',
        year: '',
    };

    public year_months: Object[] = [];

    constructor(
        private releaseService: ReleaseService,
        private waveplayerService: WavePlayerService,
        private genreService: GenreService,
        private authService: AuthService,
        private toastService: ToastrService,
        private modalService: BsModalService,
        private _router: Router,
    ) {
        this.decades = ReleaseConstants.decades;
        this.versions = ReleaseConstants.versions;
        this.bpm = ReleaseConstants.bpm;
        this.letters = ReleaseConstants.letters;
        this.dates = ReleaseConstants.dates;
        this.sort_options = ReleaseConstants.sort_options;

        let d = new Date();

        this.current_month['month'] = d.getMonth() + 1;
        this.current_month['year'] = d.getFullYear();

	    let context = this;

        this.authService.loginAnnounce$
            .subscribe(
                () => {
                    $.each(context.release_groups, function(key, group) {
                        context.processDownloadFlags(group['releases'], context.authService.user);
                    });
                }
            );

        this.authService.logoutAnnounce$
            .subscribe(
                () => {
                    this.resetDownloadFlags();
                }
            );
    }

    ngOnInit() {
        this.genreService.loadGenres();
            // .then((resp) => console.log(resp));

        this.config.currentPage = 1;
        this.none_found = false;

        this.requestYearMonth();
        this.requestReleases();

        if (this.releaseService.isFilterModified) {
            this.displaySearchParams(
                this.releaseService.getFilterParams()
            );
        }
    }

    ngAfterViewInit() {}

    public artistClick(artistName: string) {
        this.releaseService.setSearch(artistName);
        this._router.navigate(['releases']);
        this.requestReleases();
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    }

    get isLoggedIn(): Boolean {
        return this.authService.isLoggedIn;
    }

    get userHasDropbox(): Boolean {
        return this.authService.user['dropbox'] !== null;
    }

    loadPage = (page: number) => {
        this.config.currentPage = page;
        this.releases = [];

        this.requestReleases();
    }

    expandRelease = (evt: Event, id: Number) => {
        evt.preventDefault();

        if (id === this.open_release) {
            this.open_release = 0;
        } else {
            this.open_release = id;
        }
    }

    collapseRelease = (evt: Event, id: Number) => {
        evt.preventDefault();

        this.open_release = 0;
    }

    requestPlayRelease = (evt: Event, release) => {
        evt.preventDefault();

        this.waveplayerService.requestRelease(release.id);
        this.waveplayerService.requestDetail(release);
    }

    requestPlayVersion = (evt: Event, id: Number, release) => {
        evt.preventDefault();

        this.waveplayerService.requestVersion(id);
        this.waveplayerService.requestDetail(release);
    }

    requestDownloadRelease = (release: number) => {
        this.releaseService.requestReleaseCode(release)
            .then((r) => {
                if (r.code) {
                    $('#download_container').attr('src', environment.crateweb + '/download/' + r.code);
                }
            });
    }

    requestDownloadVersion = (release: number, version: number) => {
        this.releaseService.requestReleaseCode(release, version)
            .then((r) => {
                if (r.code) {
                    $('#download_container').attr('src', environment.crateweb + '/download/' + r.code);
                }
            });
    }

    requestDropboxRelease = (release: number) => {
        if (this.authService.user['dropbox'] === null) {
            this.dropboxModal.show();
        } else {
            this.releaseService.requestDropbox(release)
                .subscribe(
                    (success) => {
                        this.toastService.success('Your request has been successfully added to your Dropbox account', '', {
                            disableTimeOut: false,
                            timeOut: 3000
                        });
                    },
                    (error) => {}
                );
        }
    }

    requestDropboxVersion = (release: number, version: number) => {
        if (this.authService.user['dropbox'] === null) {
            this.dropboxModal.show();

        } else {
            this.releaseService.requestDropbox(release, version)
                .subscribe(
                    (success) => {
                        this.toastService.success('Your request has been successfully added to your Dropbox account', '', {
                            disableTimeOut: false,
                            timeOut: 3000
                        });
                    },
                    (error) => {}
                );
        }
    }

    closeDropboxModal = () => {
        this.dropboxModal.hide();
    }

    requestCrateQRelease = async (release: number, versions: any, isCrateQ: boolean) => {
        if (isCrateQ) {
            let cnt = 0;
            for (let version of versions) {
                if (version.crateq === 0) {
                    cnt++;                    
                } else {
                    const res = await this.releaseService.removeQueueVersion(version.id);
                    if (res.id != undefined) {
                        version.crateq = res.id;
                        if (res.id === 0) {
                            cnt++;
                        }
                    }
                }
            }
            if (cnt > 0) {
                let song: ReleaseItem = this.findRelease(release);
                song.crateq = false;
                if (cnt === versions.length) {
                    this.toastService.success('The song was successfully removed from crateQ queue', '', {
                        disableTimeOut: false,
                        timeOut: 3000
                    });
                }    
            }
        } else {
            const response = await this.releaseService.requestCrateQ(release);
            if (response.success) {
                let cnt = 0;
                for (let version of versions) {
                    if (version.crateq > 0) {
                        cnt++;
                    } else {
                        const res = await this.releaseService.addQueueVersion(version.id);
                        if (res.id != undefined) {
                            version.crateq = res.id;
                            if (res.id > 0) {
                                cnt++;
                            }
                        }
                    }
                }
                if (cnt === versions.length) {
                    let song: ReleaseItem = this.findRelease(release);
                    song.crateq = true;
                }
                this.toastService.success(response.msg, '', {
                    disableTimeOut: false,
                    timeOut: 3000
                });
            }
        }
    }

    requestCrateQVersion = async (release: number, version: number, vCrateQ: number) => {
        if (vCrateQ > 0) {
            const res = await this.releaseService.removeQueueVersion(version);
            if (res.id != undefined) {
                let song: ReleaseItem = this.findRelease(release);
                let cnt = 0;
                for (let v of song.versions) {
                    if (v.id === version) {
                        v.crateq = res.id;
                    }
                    if (v.crateq === 0) {
                        cnt++;
                    }
                }
                if (cnt > 0) {
                    song.crateq = false;
                }
            }
        } else {
            const response = await this.releaseService.requestCrateQ(release, version);
            if (response.success) {
                const res = await this.releaseService.addQueueVersion(version);
                if (res.id != undefined) {
                    let song: ReleaseItem = this.findRelease(release);
                    let cnt = 0;
                    for (let v of song.versions) {
                        if (v.id === version) {
                            v.crateq = res.id;
                        }
                        if (v.crateq > 0) {
                            cnt++;
                        }
                    }
                    if (cnt === song.versions.length) {
                        song.crateq = true;
                    }
                    this.toastService.success(response.msg, '', {
                        disableTimeOut: false,
                        timeOut: 3000
                    });    
                }
            }    
        }
    }

    selectYearMonth = (selection: any) => {

        let ym = selection['id'].split('|');

        this.current_month['month'] = ym[0];
        this.current_month['year'] = ym[1];

        this.requestReleases();
    }

    rateRelease = async (id, rating) => {
        let release: ReleaseItem = this.findRelease(id);

        this.releaseService.rateRelease(id, rating)
            .then((r) => {
                // Update the object with the new rating information
                release.ratings = r.ratings;
                release.score = r.score;
                release.rating = r.rating;
                release.rated = true;

                // Set this release as the open release
                this.open_release = r.id;

                if (r.crateq) {
                    this.releaseService.CQDownloads = r.crateq;
                }
            });

        let cnt = 0;
        for (const v of release.versions) {
            const res = await this.releaseService.checkQueueVersion(v.id);
            v.crateq = res.id;
            if (res.id > 0) {
                cnt++;
            }
        }
        release.crateq = release.versions.length === cnt;
    }

    removeParam = (type: string, key: any, index: number) => {
        // Modify search parameters
        this.filter_params.splice(index, 1);

        // Modify search filter
        let filter = this.releaseService.getFilterParams();

        if (key === false) {
            filter[type] = false;

        } else {
            filter[type].splice(key, 1);
        }

        // Update search filter
        this.releaseService.setFilter(filter);

        // Reload releases
        this.requestReleases();
    }

    openGroup = (group: Object) => {
        let isOpen: Boolean = group['open'];
        group['open'] = !isOpen;
    }

    openRemix = (item: ReleaseItem) => {
        const remix: ReleaseItem = {
            ...item.remixes,
            image: environment.apiURL + '/images/src/' + item.remix,
        }
        this.openDetailModal(remix);
    }

    openDetail = (item: ReleaseItem) => {
        this.openDetailModal(item);
    }
    
    private openDetailModal(release) {
        this.detailFromModalRef = this.modalService.show(DetailFormComponent, { class: 'detail-form' });
        this.detailFromModalRef.content.id = release.id;
        this.detailFromModalRef.content.img_src = release.image;
        this.detailFromModalRef.content.release_detail = release;
    }

    private requestYearMonth = () => {
        this.releaseService.getReleaseMonths()
            .then((resp) => {
                let self = this;

                resp.forEach((ym) => {
                    let formatted = {};

                    formatted['id'] = ym.month + '|' + ym.year;
                    formatted['text'] = ym.format;

                    self.year_months.push(formatted);
                });
            });
    }

    private requestReleases = () => {
        let context = this;

        this.page_title =
            this.releaseService.page_title === '' ?
            this.page_title :
            this.releaseService.page_title;

        this.releaseService.getNewReleases(
            this.current_month['month'],
            this.current_month['year']
        ).then((r) => {

            context.config.totalItems = r.total;
            context.release_groups = r.groups;

            context.none_found = (r.total === 0);

            $.each(context.release_groups, function(key, value) {
                value['open'] = false;

                $.each(value['releases'], function(rkey, rvalue) {
                    let releaseItem: ReleaseItem = context.release_groups[key]['releases'][rkey];

                    releaseItem.image = environment.apiURL + '/images/src/' + releaseItem.id;
                });

                context.processDownloadFlags(value['releases'], context.authService.user);
            });

            // Set first day's group to be open
            if (context.release_groups[0] instanceof Object) {
                context.release_groups[0]['open'] = true;
            }
        });
    }

    private processDownloadFlags = (releases: Array<ReleaseItem>, user: Object) => {
        if (user === undefined || user['releases'] == undefined) {
            return false;
        }
        
	    releases.forEach((r) => {
            r.downloaded = user['releases'].indexOf(r.id) !== -1;

            r.versions.forEach((v) => {
                v.downloaded = user['versions'].indexOf(v.id) !== -1;
            });
        });
    }

    private resetDownloadFlags = () => {
        this.releases.forEach((r) => {
            r.downloaded = false;

            r.versions.forEach((v) => {
                v.downloaded = false;
            });
        });
    }

    private displaySearchParams = (params) => {
        // Add Genre parameters
        params['search'].forEach((param, key) => {
            this.filter_params.push({
                text: param,
                type: 'search',
                key: key
            });
        });

        // Add Genre parameters
        params['genre_id'].forEach((param, key) => {
            this.genreService.genres.forEach((g) => {
                if (g['id'] === param) {
                    this.filter_params.push({
                        text: g['name'],
                        type: 'genre_id',
                        key: key
                    });
                }
            });
        });

        // Add Subgenre parameters
        params['subgenre_id'].forEach((param, key) => {
            this.genreService.genres.forEach((g) => {
                g['subgenres'].forEach((sg) => {
                    if (sg['id'] === param) {
                        this.filter_params.push({
                            text: sg['name'],
                            type: 'subgenre_id',
                            key: key
                        });
                    }
                });
            });
        });

        // Add Decade parameters
        params['decade'].forEach((param, key) => {
            this.decades.forEach((d) => {
                if (d['year'] === param) {
                    this.filter_params.push({
                        text: d['name'],
                        type: 'decade',
                        key: key
                    });
                }
            });
        });

        // Add Versions parameters
        params['version'].forEach((param, key) => {
            this.versions.forEach((v) => {
                if (v['id'] === param) {
                    this.filter_params.push({
                        text: v['name'],
                        type: 'version',
                        key: key
                    });
                }
            });
        });

        // Add BPM parameters
        params['bpm'].forEach((param, key) => {
            this.bpm.forEach((b) => {
                if (b['id'] === param) {
                    this.filter_params.push({
                        text: b['name'],
                        type: 'bpm',
                        key: key
                    });
                }
            });
        });

        // Add Subgenre parameters
        params['letter'].forEach((param, key) => {
            this.letters.forEach((l) => {
                if (l['letter'] === param) {
                    this.filter_params.push({
                        text: 'Artist begins with ' + l['letter'],
                        type: 'letter',
                        key: key
                    });
                }
            });
        });

        // Add Subgenre parameters
        params['date'].forEach((param, key) => {
            this.dates.forEach((d) => {
                if (d['id'] === param) {
                    this.filter_params.push({
                        text: d['name'],
                        type: 'date',
                        key: key
                    });
                }
            });
        });

        // Add Throwback parameter
        if (params['throwback'] === true) {
            this.filter_params.push({
                text: 'Throwback',
                type: 'throwback',
                key: false
            });

        } else if (params['throwback'] === false) {
            this.filter_params.push({
                text: 'Not Throwback',
                type: 'throwback',
                key: false
            });
        }

        // Add Remix parameter
        if (params['remix'] === true) {
            this.filter_params.push({
                text: 'Remix',
                type: 'remix',
                key: false
            });

        } else if (params['remix'] === false) {
            this.filter_params.push({
                text: 'Not Remix',
                type: 'remix',
                key: false
            });
        }
    }

    private findRelease = (id): ReleaseItem => {
        let selectedRelease: ReleaseItem = null;

        this.release_groups.forEach((group: Object) => {
            group['releases'].forEach((release: ReleaseItem) => {
                if (release.id === id) {
                    selectedRelease = release;
                }
            });
        });

        return selectedRelease;
    }
}
