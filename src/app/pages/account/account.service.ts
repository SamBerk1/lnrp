import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';

import { environment } from '../../../environments/environment';

import { JWTObject } from '../../services/jwtobject';
import { HTTPInterceptor } from '../../services/interceptor.service';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import { UserAccount } from '../../utils';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AccountService {
    constructor(
        private newHttp: HTTPInterceptor,
        private http: Http,
        private _toastService: ToastrService,
    ) { }

    updateAccount = (jwt: Object) => {
        return this.newHttp
            .put(`${environment.apiURL}/user`, jwt)
            .do(
                (res) => { },
                (err) => this.handleError(err),
                () => { }
            );
    }

    updateCard = (cardInfo: Object, user: any) => {
        return this.newHttp
            .put(`${environment.apiURL}/user/card/${user['id']}`, cardInfo)
            .map((response) => response = response.json())
            .catch(this.handleError);
    }

    unlinkDropbox = (jwt: Object) => {
        return this.newHttp
            .post(`${environment.apiURL}/user/unlink`, jwt)
            .map((response) => response = response.json())
            .catch(this.handleError);
    }

    unlinkDropboxAccount = (account) => {
        return this.unlinkDropbox(account).toPromise().then((result) => {
            this.revokeDropbox(account['dropbox']);
            this._toastService.success(result['msg']);
          });    
      }

    getDropboxInfo = (dropboxToken: string) => {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Authorization', 'Bearer ' + dropboxToken);
        return this.http.post(environment.dropboxAccount, null, { headers: headers })
            .map((response) => response = response.json())
            .catch(this.handleError);
    }

    revokeDropbox = (dropboxToken: string) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + dropboxToken);
        let httpBody = {
            'url': 'https://www.dropbox.com/s/2sn712vy1ovegw8/Prime_Numbers.txt?dl=0'
        }
        return this.http.post(environment.revokeDropbox, httpBody, { headers: headers })
            .catch(this.handleError);
    }

    private handleError = (error: any) => {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    public getCurrentAccount(): UserAccount {
        return (this.newHttp.jwt).getAccount() as UserAccount;
    }

    // returns false if any of these fields are blank
    djProfileIsConsideredComplete(account: UserAccount): boolean {

        const optedOut = localStorage.getItem('lnrp-user-profile-opt-out') === 'true'

        if (optedOut) {
            return true;
        }

        return !((!account.equipment_mac && !account.equipment_pc && !account.equipment_turntables)
            || !account.software
            || !account.controller
            || !account.type
            || !account.topgenres
            || !account.topartists
            || !account.topproducers
            || !account.influences
            || !account.desertisland
            || !account.referral_type
            || !account.referral_value);
    }

    userOptOutOfCompletingProfile() {
        localStorage.setItem('lnrp-user-profile-opt-out', 'true');
    }
}
