import { Component, ViewChild, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ModalDirective } from 'ngx-bootstrap';
import { FileUploader, FileItem } from 'ng2-file-upload';

import { environment } from '../../../environments/environment';

import { AuthService } from '../auth/auth.service';
import { AccountService } from './account.service';
import { JWTObject } from '../../services/jwtobject';
import { HTTPInterceptor } from '../../services/interceptor.service';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

const uploadURL = `${environment.apiURL}/images/upload`;

@Component({
    selector: 'account',
    templateUrl: './account.component.html'
})
export class AccountComponent implements OnInit {
    @ViewChild('accountModal')
    public accountModal: ModalDirective;

    @ViewChild('paymentModal')
    public paymentModal: ModalDirective;

    @ViewChild('accountForm')
    public accountForm: NgForm;

    jwt: JWTObject;
    account: any = {};

    display: any = {
        advertisement: false
    };

    refresh: string = 'nothing';

    hasDropZoneOver: boolean = false;
    uploader: FileUploader;

    constructor(
        private authService: AuthService,
        private newHttp: HTTPInterceptor,
        private accountService: AccountService,
        private toastService: ToastrService,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
        this.account = <JWTObject>(this.newHttp.jwt).getAccount();

        if (this.account === undefined) {
            this.router.navigateByUrl('/');
            return;
        }

        // Set up a blank password field, in case they want to update it
        this.account.password = '';

        // Correct the user image to API standards
        this.account['image'] = `${environment.apiURL}/user/image/${this.account['id']}`;

        this.uploader = new FileUploader({
            autoUpload: true,
            authToken: 'Bearer ' + this.newHttp.jwt.toJWT(),
            removeAfterUpload: true,
            url: uploadURL
        });

        // Hack to fix CORS problem...
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onSuccessItem = this.onSuccessfulUpload;

        if (this.account['dropbox']) {
            this.getDropboxAccount();
        }
    }

    get dropboxConnected() {
        return this.account['dropbox'] !== null;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            if (params['refresh'] === 'dropbox') {
                this.refreshAccount();
            }
        });

        this.authService.logoutAnnounce$
            .subscribe(
                () => {
                    this.router.navigateByUrl('/');
                }
            );
    }

    openAccountModal = () => {
        this.accountModal.show();
    }

    closeAccountModal = () => {
        this.accountModal.hide();
    }

    openPaymentModal = () => {
        this.paymentModal.show();
    }

    closePaymentModal = () => {
        this.paymentModal.hide();
    }

    openDropboxLink = () => {
        window.location.href = environment.dropboxweb + '/dropbox/connect/' + this.account['id'];
    }

    unlinkDropboxAccount = () => {
        this.accountService.unlinkDropboxAccount(this.account).then(result => {
            this.refreshAccount();
        });
    }

    refreshAccount = () => {
        this.authService.refreshUserToken().subscribe((success) => {
            this.jwt = this.newHttp.jwt;

            this.account = this.jwt['payload'];

            // Clear the password field, for reasons
            this.account['password'] = '';

            // Correct the user image to API standards
            this.account['image'] = `${environment.apiURL}/user/image/${this.account['id']}`;

            if (this.account['dropbox']) {
                this.getDropboxAccount();
            }
        });
    }

    getDropboxAccount() {
        this.accountService.getDropboxInfo(this.account['dropbox']).subscribe((res) => {
            this.account['dropbox_info'] = res;
        }, (err) => {
            err = err.json();
            console.log('---', err);
            if (err.error['.tag'] == 'invalid_access_token') {
                this.accountService.unlinkDropbox(this.account).subscribe((result) => {
                    this.refreshAccount();
                });        
            }
        });
    }

    onSubmit = (form: any) => {
        this.accountService.updateAccount(this.account)
            .subscribe(
                (result) => {
                    this.newHttp.jwt['payload'] = this.account;

                    // Clear the password field, for reasons
                    this.account['password'] = '';

                    // Correct the user image to API standards
                    this.account['image'] = `${environment.apiURL}/user/image/${this.account['id']}`;

                    this.toastService.success('Your account information has been updated!');

                    this.accountModal.hide();
                },
                (err) => {
                    console.error(err);
                },
                () => {}
            );
    }

    updateCard = (form: any) => {
        this.accountService.updateCard(form.value, this.account)
            .subscribe(
                (response) => {
                    this.account = response['user'];

                    // Clear the password field, for reasons
                    this.account['password'] = '';

                    // Correct the user image to API standards
                    this.account['image'] = `${environment.apiURL}/user/image/${this.account['id']}`;

                    this.newHttp.updateJWT(response['_t']);

                    this.toastService.success('Your card has been updated!');
                    this.closePaymentModal();
                },
                (err) => {
                    console.error(err);
                }
            );
    }

    fileOver = (evt: any) => {
        this.hasDropZoneOver = evt;
    }

    openCrateQDowload() {
        window.open(environment.crateQDownloader, '_blank');
    }

    goToProfile = () => {
        this.router.navigateByUrl('/profile');
    }

    private onSuccessfulUpload = (item: FileItem, response: string) => {
        this.toastService.success('Your new user image has been updated successfully!');
        this.account['image'] = `${environment.apiURL}/user/image/${this.account['id']}/150/150?random=${Math.random()}`;
    }
}
