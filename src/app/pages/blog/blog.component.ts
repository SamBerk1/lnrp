import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'blog-page',
    templateUrl: './blog.component.html',
    encapsulation: ViewEncapsulation.None
})
export class BlogComponent {

    url: string;
    constructor() {
        this.url = 'https://blog.latenightrecordpool.com';
    }
}
