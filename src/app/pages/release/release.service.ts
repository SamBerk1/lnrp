import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions } from '@angular/http';
import { ToastrService } from 'ngx-toastr';

import { HTTPInterceptor } from '../../services/interceptor.service';
import { AuthService } from '../auth/auth.service';

import { environment } from '../../../environments/environment';

import { ReleaseSearchFilter } from '../../utils';
import { SearchTermRules } from '../../utils';

import { ErrorHelper, JWTObject } from '../../services';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

export interface CrateQLink {
    version: string;
    win: string;
    mac: string;
}

@Injectable()
export class ReleaseService {
    private releaseReloadSource = new Subject<Number>();
    private clearSearchTextSource = new Subject<Number>();
    private crateqModalSource = new Subject<any>();

    releaseReload$ = this.releaseReloadSource.asObservable();
    clearSearchTextReload$ = this.clearSearchTextSource.asObservable();
    crateqModal$ = this.crateqModalSource.asObservable();

    private searchFilter: ReleaseSearchFilter = new ReleaseSearchFilter();

    // Can sort by: artist, title, bpm, rating, date, or release id
    private searchSort: string = '';

    page_title: string = '';

    labels: Object[] = [];

    user_id: number;
    ip_address: string = '';

    constructor(
        private newHttp: HTTPInterceptor,
        private authService: AuthService,
        private toastService: ToastrService
    ) {
        this.fetchUserId();
        this.authService.loginAnnounce$
            .subscribe(() => this.fetchUserId());

        this.authService.logoutAnnounce$
            .subscribe(() => {
                localStorage.removeItem('lnrp-user');
                this.user_id = null;
            });
        
        this.ip_address = localStorage.getItem('lnrp-ip');
    }

    get isFilterModified(): Boolean {
        return this.searchFilter.isModified;
    }

    getAllReleases = (start: number, length: number) => {
        // Build out search URL
        let url = environment.apiURL + '/releases/' + start + '/' + length;

        return this.newHttp.post(url, this.addSearchTermsIntelligence(this.searchFilter.params))
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    // trying to add some "intelligence" to search terms
    public addSearchTermsIntelligence = (searchParams: any) => {
        if (searchParams && searchParams.search && searchParams.search.length > 0) {
            searchParams.search = searchParams.search.map(function(searchTerm) {
                SearchTermRules.forEach(rule => {
                    searchTerm = (searchTerm as string).replace(rule.find, rule.replace);
                });
                return searchTerm;
            });
        }
        return searchParams;
    }

    getNewReleases = (month: number, year: number) => {
        // Build out search URL
        let url = environment.apiURL + '/releases/new/' + month + '/' + year;

        return this.newHttp.get(url)
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    getReleaseMonths = () => {
        return this.newHttp.get(environment.apiURL + '/releases/months')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    getReleaseDetail = (id: number) => {
        return this.newHttp.get(environment.apiURL + '/release/detail/' + id)
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    getTotalReleaseCount = () => {
        return this.newHttp.get(environment.apiURL + '/releases/count')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    getTopDownloads = (genre: number, subgenre: number) => {
        let url = environment.apiURL + '/top';

        if (genre !== -1 && subgenre !== -1) {
            url += '/' + genre + '/' + subgenre;

        } else if (genre !== -1 && subgenre === -1) {
            url += '/' + genre;
        }

        return this.newHttp.get(url)
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    getRecentTopDownloads = () => {
        const url = environment.apiURL + '/recent';
        return this.newHttp.get(url)
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    loadLabels = () => {
        if (this.labels.length === 0) {
            this.newHttp.get(environment.apiURL + '/labels')
                .toPromise()
                .then((labels) => this.labels = labels.json());
        }
    }

    rateRelease = (id, rating) => {
        return this.newHttp.post(environment.apiURL + '/release/rate/' + id + '/' + rating, '')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    requestReleaseCode = (release: number, version?: number) => {
        let url: string = environment.apiURL + '/download/request/' + release;

        if (version != undefined) {
            url += '/' + version;
        }

        return this.newHttp.post(url, '')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    requestDropbox = (release: number, version?: number) => {
        let url: string = environment.apiURL + '/dropbox/download/' + release;

        if (version != undefined) {
            url += '/' + version;
        }

        return this.newHttp.post(url, '')
                   .map(response => response.json())
                   .catch(this.handleError);
    }

    checkQueueVersion = (version: number) => {
        const url: string = `${environment.queueAPIURL}/CheckQueue?key=${environment.crateqKey}&userId=${this.user_id}&originatorId=${version}`;
        return this.newHttp.get(url)
                    .toPromise()
                    .then(response => response.json())
                    .catch(this.handleError);
    }

    addQueueVersion = (version: number) => {
        const url: string = `${environment.queueAPIURL}/AddToQueue?key=${environment.crateqKey}`;
        const reqBody: any = {
            originatorId: version,
            userId: this.user_id,
            ip: this.ip_address,
            source: 'lnrp',
            queuedFrom: 'lnrp'
        };
        return this.newHttp.post(url, JSON.stringify(reqBody))
                    .toPromise()
                    .then(response => response.json())
                    .catch(this.handleError);
    }

    removeQueueVersion = (version: number) => {
        const url: string = `${environment.queueAPIURL}/RemoveFromQueue?key=${environment.crateqKey}`;
        const reqBody: any = {
            originatorId: version,
            userId: this.user_id,
            ip: this.ip_address,
            source: 'lnrp',
            queuedFrom: 'lnrp'
        };
        return this.newHttp.post(url, JSON.stringify(reqBody))
                    .toPromise()
                    .then(response => response.json())
                    .catch(this.handleError);
    }

    requestCrateQ = (release: number, version?: number) => {
        let url: string = `${environment.apiURL}/download/crateq/${release}`;

        if (version != undefined) {
            url += `/${version}`;
        }

        if (!this.isCQDownloaded) {
            this.crateqModalSource.next();
        }

        return this.newHttp.post(url, '').toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    setFilter = (filter: Object) => {
        this.searchFilter = new ReleaseSearchFilter(filter);
    }

    getFilterParams = (): Object => {
        return this.searchFilter.params;
    }

    setSearch = (term: string | string[]) => {
        if (typeof term === 'string') {
            this.searchFilter.params = {
                search: [term]
            };

        } else {
            this.searchFilter.params = {
                search: term
            };
        }
    }

    setGenre = (id: Number | Number[], genre: string) => {
        if (typeof id === 'number') {
            this.searchFilter.params = {
                genre_id: [id]
            };

        } else {
            this.searchFilter.params = {
                genre_id: id
            };
        }

        this.page_title = 'All ' + genre + ' Releases';
    }

    setDecade = (decade: Number | Number [], year: string) => {
        if (typeof decade === 'number') {
            this.searchFilter.params = {
                decade: [decade]
            };

        } else {
            this.searchFilter.params = {
                decade: decade
            };
        }

        this.page_title = 'All Releases From The ' + year;
    }

    setThrowback = (id: Number | Number [], genre: string) => {
        if (typeof id === 'number') {
            this.searchFilter.params = {
                genre_id: [id],
                throwback: true
            };

        } else {
            this.searchFilter.params = {
                genre_id: id,
                throwback: true
            };
        }

        this.page_title = 'All ' + genre + ' Throwback Releases';
    }

    setLabel = (label: string | string[]) => {
        if (typeof label === 'string') {
            this.searchFilter.params = {
                label: [label]
            };

        } else {
            this.searchFilter.params = {
                label: label
            };
        }

        this.page_title = 'All Releases From ' + label;
    }

    setSort = (sortValue: string) => {
        this.searchFilter.sort = sortValue;
    }

    clearFilter = () => {
        this.searchFilter.params = {};
        this.searchFilter.sort = '';

        this.page_title = '';

        this.requestClearSearchText();
    }

    requestReleasesBroadcast = () => {
        this.releaseReloadSource.next();
    }

    requestClearSearchText = () => {
        this.clearSearchTextSource.next();
    }

    private handleError = (error: any) => {
        if (!error.ok) {
            switch (error.status) {
                case 400:
                    this.showNotification('An error occurred trying to perform this action. Please try again or contact support.', 'error');
                    break;

                case 401:
                    this.authService.registerLogout();
                    this.showNotification('Your login has expired. Please log back in and try again.', 'warn')
                    break;

                case 404:
                    this.showNotification(error._body, 'error');
                    break;

                default:
                    // Do nothing, yet.
                    break;
            }
        }

        return Observable.throw(error._body || error);
    }

    showNotification(message: string, type: string) {
        if (type == 'error') {
            // error message
            this.toastService.error(message);
        } else if (type == 'warn') {
            // warning message
            this.toastService.warning(message);
        } else if (type == 'success') {
            // success message
            this.toastService.success(message);
        }
    }

    fetchLandingContent = () => {
        return this.newHttp.get(environment.apiURL + '/content/landing')
                   .toPromise()
                   .then(response => response.json())
                   .catch(this.handleError);
    }

    fetchDownloadLink = () => {
        return this.newHttp.get(environment.apiURL + '/crateq/download')
                   .toPromise()
                   .then(response => response.json())
                   .catch((error) => {
                       console.log('download link failed', error);
                   });
    }

    private fetchUserId = () => {
        if (localStorage.getItem('_t')) {
            this.newHttp.updateJWT(
                localStorage.getItem('_t')
            );
            const account = <JWTObject>(this.newHttp.jwt).getAccount();
            localStorage.setItem('lnrp-user', account['id']);
            this.user_id = parseInt(account['id']);
        } else {
            localStorage.removeItem('lnrp-user');
            this.user_id = 0;
        }
    }

    get isCQDownloaded(): Boolean {
        const CQDownloads = Number(localStorage.getItem('lnrp-crateqs'));
        return CQDownloads > 0;
    }

    set CQDownloads(crateQs) {
        localStorage.setItem('lnrp-crateqs', crateQs);
    }
}
