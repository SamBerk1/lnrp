import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { environment } from '../../../environments/environment';
import { AuthService } from '../auth/auth.service';
import { GenreService } from '../../services/genre.service';
import { ReleaseService } from '../release/release.service';
import { WavePlayerService } from '../waveplayer/waveplayer.service';
import { DetailFormComponent } from '../detail-form/detail-form.component';
import * as $ from 'jquery';

@Component({
    selector: 'top-download',
    templateUrl: './top.component.html'
})

export class TopDownloadComponent implements OnInit, AfterViewInit {
    public _genre: number = 0;
    public _subgenre: number = 0;

    public first_row: Object[];
    public second_row: Object[];
    public third_row: Object[];

    public releases: Object[];
    public allReleases: Object[];

    releaseDetails = [];
    top_title: string = 'All Genres';

    detailFromModalRef: BsModalRef;

    constructor(
        private route: ActivatedRoute,
        private releaseService: ReleaseService,
        private waveplayerService: WavePlayerService,
        private authService: AuthService,
        private modalService: BsModalService,
        private _router: Router,
    ) { }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this._genre = +params['genre'] || -1;
            this._subgenre = +params['subgenre'] || -1;

            this.requestTopDownloads(this._genre, this._subgenre);
        });
        this.authService.loginAnnounce$.subscribe(() => {
            this.prearrangeData();
        });
        this.authService.logoutAnnounce$.subscribe(() => {
            this.prearrangeData();
        });
    }

    public artistClick(artistName: string) {
        this.releaseService.setSearch(artistName);
        this._router.navigate(['releases']);
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    }

    ngAfterViewInit() { }

    get isLoggedIn(): Boolean {
        return this.authService.isLoggedIn;
    }

    triggerLoginModal = () => {
        this.authService.openLoginModal();
    }

    requestPlayRelease = (evt: any, release) => {
        evt.preventDefault();

        this.waveplayerService.requestRelease(release.id);
        if (this.releaseDetails[release.number - 1]) {
            this.releaseDetails[release.number - 1]['image'] = release.image;
            this.waveplayerService.requestDetail(this.releaseDetails[release.number - 1]);
        }
    }

    openDetail(ind: number) {
        if (this.releaseDetails[ind - 1]) {
            this.detailFromModalRef = this.modalService.show(DetailFormComponent, { class: 'detail-form' });
            this.detailFromModalRef.content.id = this.releaseDetails[ind - 1].id;
            this.detailFromModalRef.content.img_src = environment.apiURL + '/images/src/' + this.releaseDetails[ind - 1].id;
            this.detailFromModalRef.content.release_detail = this.releaseDetails[ind - 1];
        }
    }

    private requestTopDownloads = (genre: number, subgenre: number) => {
        this.releaseService.getTopDownloads(genre, subgenre)
            .then((response) => {
                if (response['genre'] !== false && response['subgenre'] !== false) {
                    this.top_title = response['genre'] + ' > ' + response['subgenre'];
                } else if (response['genre'] !== false && response['subgenre'] === false) {
                    this.top_title = response['genre'];
                }

                this.allReleases = response.releases;

                this.allReleases.forEach((r, key) => {
                    r['image'] = environment.apiURL + '/images/src/' + r['id'];
                    r['number'] = key + 1;
                });

                this.prearrangeData();
                this.getDetails();
            });
    }

    private prearrangeData() {
        this.releases = [];
        this.allReleases.forEach(item => {
            this.releases.push(item);
        });
        this.first_row = this.releases.splice(
            0,
            this.isLoggedIn ? 2 : 1
        );

        this.second_row = this.releases.splice(0, 2);
        this.third_row = this.releases.splice(0, 2);
    }

    private getDetails() {
        let promises = [];
        this.allReleases.forEach(item => {
            const promise = this.releaseService.getReleaseDetail(item['id']);
            promises.push(promise);
        });
        Promise.all(promises).then(result => {
            this.releaseDetails = result;
        }, errors => {
            console.log('Get detail error', errors);
        });
    }
}
