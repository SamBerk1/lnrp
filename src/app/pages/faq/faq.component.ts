import { Component, OnInit } from '@angular/core';

import { GeneralService } from '../../services';

@Component({
    selector: 'faq',
    templateUrl: './faq.component.html'
})

export class FAQComponent implements OnInit {
    pageTitle: string = 'FAQ';
    pageContent: string = '';

    constructor(
        private generalService: GeneralService
    ) {}

    ngOnInit() {
        this.generalService.getFAQContent()
            .subscribe(
                (success) => {
                    this.pageTitle = success.title;
                    this.pageContent = success.content;
                }
            );
    }
}
