import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SlimLoadingBarModule, SlimLoadingBarEventType } from 'ng2-slim-loading-bar';
import { ModalModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SelectModule } from 'ng2-select';
import { NgxPaginationModule } from 'ngx-pagination';
import { FileUploadModule } from 'ng2-file-upload';
import { ReCaptchaModule } from 'angular2-recaptcha';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { RatingModule } from './modules/ratings.module';

import { AuthService } from './pages/auth/auth.service';
import { FrontpageService } from './pages/frontpage/frontpage.service';
import { ReleaseService } from './pages/release/release.service';
import { WavePlayerService } from './pages/waveplayer/waveplayer.service';
import { SignupService } from './pages/signup/signup.service';
import { AccountService } from './pages/account/account.service';
import { ResetPasswordService } from './pages/password/password.service';
import { AudioWaveformModule } from './pages/waveplayer/waveform.module';
import { VolumeControlModule } from './pages/waveplayer/volume.module';

import {
    SanitizerPipe,
    SanitizerHTMLPipe,
    SanitizerURLPipe,
    TimePipe,
    SlugPipe
} from './pipes';

import {
    INTERCEPTOR_PROVIDERS,
    ERROR_HELPER_PROVIDERS,
    GenreService,
    RouteService,
    GeneralService
} from './services';

import {
    FrontpageComponent,
    TopDownloadComponent,
    NewReleaseComponent,
    ReleaseComponent,
    PricingComponent,
    SignupComponent,
    AccountComponent,
    AuthComponent,
    ContactComponent,
    FAQComponent,
    RenewAccountComponent,
    AdvancedSearchComponent,
    ResetPasswordComponent,
    WavePlayerComponent,
    BlogComponent,
    TrendComponent,
    DetailFormComponent,
    PromoModalComponent,
    ProfileComponent,
} from './pages';

import { appRouting } from './app.routes';
import { SignupDisclaimerModalComponent } from './pages/signup/signup-disclaimer-modal/signup-disclaimer-modal.component';
import { FeaturesComponent } from './pages/features/features.component';
import { ProfileWelcomeModalComponent } from './pages/profile/profile-welcome-modal/profile-welcome-modal.component';

@NgModule({
    declarations: [
        AppComponent,
        FrontpageComponent,
        AuthComponent,
        TopDownloadComponent,
        NewReleaseComponent,
        ReleaseComponent,
        PricingComponent,
        SignupComponent,
        AccountComponent,
        ContactComponent,
        FAQComponent,
        RenewAccountComponent,
        AdvancedSearchComponent,
        ResetPasswordComponent,
        WavePlayerComponent,
        DetailFormComponent,
        BlogComponent,
        TrendComponent,
        SanitizerPipe,
        SanitizerHTMLPipe,
        SlugPipe,
        SanitizerURLPipe,
        TimePipe,
        PromoModalComponent,
        ProfileComponent,
        SignupDisclaimerModalComponent,
        FeaturesComponent,
        ProfileWelcomeModalComponent,
    ],
    imports: [
        BrowserModule,
        HttpModule,
        appRouting,
        FormsModule,
        RatingModule,
        SharedModule,
        SelectModule,
        ReCaptchaModule,
        FileUploadModule,
        VolumeControlModule,
        AudioWaveformModule,
        NgxPaginationModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            disableTimeOut: true,
            closeButton: true,
            tapToDismiss: false,
            preventDuplicates: true,
            positionClass: 'toast-top-full-width'
        }),
        ModalModule.forRoot(),
        SlimLoadingBarModule.forRoot(),
    ],
    providers: [
        INTERCEPTOR_PROVIDERS,
        ERROR_HELPER_PROVIDERS,
        AuthService,
        FrontpageService,
        ReleaseService,
        WavePlayerService,
        GenreService,
        GeneralService,
        RouteService,
        SignupService,
        AccountService,
        ResetPasswordService,
    ],
    entryComponents: [
        DetailFormComponent,
        PromoModalComponent,
        SignupDisclaimerModalComponent,
        ProfileWelcomeModalComponent,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
